import React, {Component} from "react";
import {HOST, LOGIN_HOST} from "../../utils/FetchUtils.js"
import "./Login.css"
import CryptoJS from 'crypto-js';
import {  Link } from "react-router-dom";

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
        username: "",
        password: "",
        setToken: props.setToken,
        errorMessage: "",
        setUser: props.setUser
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

    handleCheckboxChange(event) {
      const target = event.target;
      const value = target.checked;
      const name = target.name;
      this.setState({
        [name]: value
      });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({
            errorMessage: ""
        })
        const user = {
          username: this.state.username,
          password: this.state.password,
          email: this.state.username,
          applicationCode: "pokeme"
        }
        if (this.state.newuser) {
            user.timezone = "America/New_York"
        }
        const pathPart = this.state.newuser ? 'user' : 'token'
        fetch(LOGIN_HOST + '/v1/' + pathPart, {
            method: 'POST',
            headers: {
               'Access-Control-Allow-Origin': '*',
               'Accept': 'application/json',
               'Content-Type': 'application/json',
            },
            body: JSON.stringify(user),
        })
        .then((response) => {
            if (response.status === 401) {
                this.setState({
                    errorMessage: "User Credentials Incorrect"
                })
            }
            if (response.status === 400) {
                this.setState({
                    errorMessage: "Username (Email) and Password Required"
                })
            }
            return response.json()
        })
        .then((data) => {
            if (data.token) {
                this.state.setToken(data.token.replaceAll('"',""))
                return data
            }
        })
        .then((extraData) => {
            return fetch(LOGIN_HOST + '/v1/user', {
                method: 'GET',
                headers: {
                   'Access-Control-Allow-Origin': '*',
                   'Authorization': extraData.token,
                   'App-Code': 'pokeme',
                   'Accept': 'application/json',
                   'Content-Type': 'application/json',
                }
            })
        })
        .then((additionalResponse) => {
            if (additionalResponse.status === 401) {
                this.setState({
                    errorMessage: "User Credentials Incorrect"
                })
            }
            if (additionalResponse.status === 400) {
                this.setState({
                    errorMessage: "Username (Email) and Password Required"
                })
            }
            return additionalResponse.json()
        })
        .then((dataUser) => {
            if (dataUser.id) {
                this.state.setUser(dataUser)
                return dataUser
            }
        })
        .catch((error) => {
            console.error('Error:', error);
            this.setState({
                errorMessage: "Failed to log in"
            })
        });
    }

  render() {
      return (
      <div className="container">
          <div className="row login-wrapper">
            <div className="d-flex justify-content-center">
                <h1 class="text-primary"><b>Please Log In</b></h1>
            </div>
          </div>
          <form onSubmit={this.handleSubmit}>
              <div className="form-row">
                    <div className="form-group col">
                          <label for="un">Email</label>
                          <input id="un" className="form-control" name="username" type="text" onChange={this.handleInputChange} />
                    </div>
                    <div className="form-group col">
                          <label for="pw">Password</label>
                          <input id="pw" className="form-control" name="password" type="password" onChange={this.handleInputChange} />
                    </div>
                    <div class="form-group col">
                        <input className="form-check-input" name="newuser" type="checkbox" id="gridCheck" onChange={this.handleCheckboxChange} />
                        <label class="form-check-label" for="gridCheck">
                          New User
                        </label>
                    </div>
              </div>
              <Link className="nav-link" to="/forgotmypassword">Forgot Password?</Link>
              <div className="form-row">
                <button type="submit" className="btn btn-primary">Submit</button>
              </div>
          </form>
        {this.state.errorMessage &&
          <div className="row">
              <h4 className="error">{this.state.errorMessage}</h4>
          </div>
        }
      </div>
      )
  }
}

export default Login
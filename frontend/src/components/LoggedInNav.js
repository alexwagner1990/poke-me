import React, { Component }  from 'react';
import {HOST, LOGIN_HOST} from '../utils/FetchUtils.js';
import {  Link } from "react-router-dom";
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { ReactSVG } from 'react-svg'
import "./Navbar.css"


class LoggedInNav extends Component {

      constructor(props) {
        super(props)
        this.state = {
            errorMessage: ""
        }
        this.logout = this.logout.bind(this);
      }

      logout() {
            console.log('Logout!')
            const tokenString = localStorage.getItem('poke-me-token');
            this.purgeToken(tokenString)
      }

      purgeToken(token) {
           if (!token) return;
           fetch(LOGIN_HOST + '/v1/token', {
               method: 'DELETE',
               headers: {
                   'Access-Control-Allow-Origin': '*',
                   'Accept': 'application/json',
                   'Content-Type': 'application/json',
                   'Authorization': token,
                   'App-Code': 'pokeme'
               }
           })
           .then((response) => {
                if (!response.ok) {
                    this.setState({
                        errorMessage: "Something went wrong, try again"
                    })
                    throw new Error("HTTP Status: " + response.status)
                }
                return response.json()
           })
           .then((data) => {
               console.log('Success:', data);
               this.props.setUser(null)
               this.props.removeToken()
           })
           .catch((error) => {
               console.error('Error:', error);
           });
      }

      render() {
          return (
            <>
                <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                  <div class="container-fluid">
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                      <div class="navbar-nav">
                        <Link className="nav-link" to="/">Home</Link>
                        <Link className="nav-link" to="/notifications">My Notifications</Link>
                        <Link className="nav-link" to="/newnotification">New Notification</Link>
                        <Link className="nav-link" to="/settings">Settings</Link>
                        <Link className="nav-link" to="/" onClick={this.logout}>Logout</Link>
                      </div>
                    </div>
                  </div>
                </nav>
                {this.state.errorMessage &&
                    <div >
                        <h4 color="red">{this.state.errorMessage}</h4>
                    </div>
                }
            </>
          );
      }
}

export default LoggedInNav;
import { Link } from "react-router-dom";
import React, {Component} from "react";
import LoggedInNav from "../components/LoggedInNav";
import gator from '../l8rg8r.png'

class Layout extends Component {

    render() {
      return (
      <div>
        <div class="home">
            <h1 class="text-primary"><b>Welcome!</b></h1>
            <img src={gator} style={{ width: 300, height: 300 }} />
            <p class="text-center">Set your timezone in the Settings section</p>
            <p class="text-center">I can send a message with text and an image/gif. Right now I can only get an image/gif from a URL (e.g. giphy)</p>
            <p class="text-center">For any questions or complaints contact Alex!</p>
        </div>
        <h3 class="text-center text-primary">Upcoming Features</h3>
        <div class="text-center">
            <h4>{`\u2022 Email Notifications`}</h4>
            <h4>{`\u2022 Dynamic messages`}</h4>
            <h4>{`\u2022 Making the site look less spartan`}</h4>
        </div>
      </div>
      )
    };

}
export default Layout;
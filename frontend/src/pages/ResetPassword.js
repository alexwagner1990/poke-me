import React, {Component} from "react";
import {HOST, LOGIN_HOST} from "../utils/FetchUtils.js"
import CryptoJS from 'crypto-js';
import {  Link } from "react-router-dom";
import { withRouter } from 'react-router-dom'

class ResetPassword extends Component {

  constructor(props) {
    super(props);
    this.state = {
        username: "",
        password: "",
        setToken: props.setToken,
        errorMessage: ""
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.setToken = this.setToken.bind(this);
  }

   handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
          [name]: value
        });
   }

   setToken(myToken) {
        this.setState({
            token: myToken
        })
        localStorage.setItem('poke-me-token', JSON.stringify(myToken))
   }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({
            errorMessage: ""
        })
        if (this.state.password !== this.state.confirmpassword) {
            this.setState({
                errorMessage: "Passwords do not match"
            })
            return
        }
        const params = new URLSearchParams(this.props.location.search)
        fetch(LOGIN_HOST + '/v1/user/password' , {
            method: 'PUT',
            headers: {
               'Access-Control-Allow-Origin': '*',
               'Accept': 'application/json',
               'App-Code': 'pokeme',
               'Content-Type': 'application/json',
               'Authorization': params.get('token'),
               'password': this.state.password
            },
        })
        .then((response) => {
            if (response.status >= 400 ) {
                this.setState({
                    errorMessage: "Unauthorized"
                })
            }
            return response.json()
        })
        .then((data) => {
            console.log('Success:', data);
            if (data.token) {
                this.setToken(data.token.replaceAll('"',""))
                window.location.href = '/'
            }
        })
        .catch((error) => {
            console.error('Error:', error);
            this.setState({
                errorMessage: "Failed to change password"
            })
        });
    }

  render() {
      return (
      <div className="container">
          <div className="row login-wrapper">
            <div className="d-flex justify-content-center">
                <h1 class="text-primary"><b>Enter New Password</b></h1>
            </div>
          </div>
          <form onSubmit={this.handleSubmit}>
              <div className="form-row">
                    <div className="form-group col">
                          <label for="pw">Password</label>
                          <input id="pw" className="form-control" name="password" type="password" onChange={this.handleInputChange} />
                    </div>
                    <div className="form-group col">
                          <label for="cpw">Confirm Password</label>
                          <input id="cpw" className="form-control" name="confirmpassword" type="password" onChange={this.handleInputChange} />
                    </div>
              </div>
              <div className="form-row">
                <button type="submit" className="btn btn-primary">Submit</button>
              </div>
          </form>
        {this.state.errorMessage &&
          <div className="row">
              <h4 className="error">{this.state.errorMessage}</h4>
          </div>
        }
      </div>
      )
  }
}

export default withRouter(ResetPassword)
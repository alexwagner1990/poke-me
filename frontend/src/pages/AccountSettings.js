import { Link } from "react-router-dom";
import React, {Component} from "react";
import LoggedInNav from "../components/LoggedInNav";
import {LOGIN_HOST} from "../utils/FetchUtils.js";
import Button from 'react-bootstrap/Button';

class AccountSettings extends Component {
  constructor(props) {
    super(props);
    const cuser = JSON.parse(localStorage.getItem('poke-me-user'))
    this.state = {
        currentuser: cuser,
        timezone: cuser.timezone ? cuser.timezone : "America/New_York",
        password: "",
        confirmpassword: "",
        errorMessage: "",
        successMessage: ""
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleLoading = this.handleLoading.bind(this);
    this.handleDropdown = this.handleDropdown.bind(this);
  }


  handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
          [name]: value
        });
  }

  handleLoading() {
      this.setState({
          isLoading: !this.state.isLoading
      })
  }

  handleSubmit(event) {
      this.handleLoading()
      event.preventDefault();
      this.setState({
        errorMessage: "",
        successMessage: ""
      })
      if (this.state.password !== this.state.confirmpassword) {
        this.setState({
            errorMessage:"Passwords Don't Match",
            isLoading: false
        })
        return
      }
      const updateUser = {
        applicationCode: "pokeme",
        password: this.state.password ? this.state.password: null,
        username: this.state.currentuser.username,
        timezone: this.state.timezone,
        email: this.state.currentuser.username
      }
      fetch(LOGIN_HOST + '/v1/user' , {
        method: 'PUT',
        headers: {
           'Access-Control-Allow-Origin': '*',
           'Accept': 'application/json',
           'App-Code': 'pokeme',
           'Content-Type': 'application/json',
           'Authorization': localStorage.getItem('poke-me-token'),
        },
        body: JSON.stringify(updateUser)
      })
      .then((response) => {
        if (response.status >= 400 ) {
            this.setState({
                errorMessage: "Unauthorized",
                isLoading: false
            })
            throw Error()
        }
        return response.json()
      })
      .then((data) => {
        console.log('Success:', data);
        if (data.token) {
            this.setState({
                successMessage: "User Updated Successfully",
                isLoading: false
            })
            localStorage.setItem('poke-me-token', data.token)
        }
        return data.token
      })
      .then((token) => {
           if (token) {
               return fetch(LOGIN_HOST + '/v1/user', {
                   method: 'GET',
                   headers: {
                       'Content-Type': 'application/json',
                       'Authorization': token,
                       'App-Code': 'pokeme'
                   }
               })
           }
           throw Error("No Valid Token")
      })
       .then((response) => {
           return response.json()
       })
       .then((user) => {
           if (user) {
               localStorage.setItem('poke-me-user', JSON.stringify(user))
           }
       })
       .catch((error) => {
           console.error('Error:', error);
       });
  }

    handleDropdown(option) {
        this.setState({
            timezone: option.target.value
        })
    }

    render() {
      return (
        <div className="container">
          <form onSubmit={this.handleSubmit}>
            <div className="form-group row">
              <div>
                <label for="phoneNumberInput" className="col-sm-2 col-form-label">Change Timezone</label>
                <div>
                    <select value={this.state.timezone} onChange={this.handleDropdown}>
                        <option value="America/New_York">Eastern</option>
                        <option value="America/Chicago">Central</option>
                        <option value="America/Denver">Mountain</option>
                        <option value="America/Los_Angeles">Pacific</option>
                    </select>
                </div>
              </div>
            </div>
            <br />
            <div className="form-row">
                <div className="form-group col">
                      <label for="pw">Change Password</label>
                      <input id="pw" className="form-control" name="password" type="password" onChange={this.handleInputChange} />
                </div>
                <div className="form-group col">
                      <label for="cpw">Confirm Password</label>
                      <input id="cpw" className="form-control" name="confirmpassword" type="password" onChange={this.handleInputChange} />
                </div>
            </div>
            <br />
            <div className="form-group row">
                <Button
                  variant="primary"
                  disabled={this.state.isLoading}
                  onClick={!this.state.isLoading ? this.handleSubmit : null}
                >
                  {this.state.isLoading ? 'Loading…' : 'Update'}
                </Button>
            </div>
          </form>
          {this.state.errorMessage &&
          <div>
            <div className="row">
                <h4 className="error">{this.state.errorMessage}</h4>
            </div>
          </div>
          }
          {this.state.successMessage &&
          <div>
            <div className="row">
                <h4 className="success">{this.state.successMessage}</h4>
            </div>
          </div>
          }
        </div>
      )
    };

}
export default AccountSettings
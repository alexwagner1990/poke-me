import { Link } from "react-router-dom";
import React, {Component} from "react";
import {HOST, LOGIN_HOST} from "../utils/FetchUtils.js"
import LoggedInNav from "../components/LoggedInNav";

class ForgotPassword extends Component {

    constructor(props) {
        super(props)
        this.state = {
            successEmailSent: false,
            failureEmail: false
        }
        this.sendEmail = this.sendEmail.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.setSuccess = this.setSuccess.bind(this);
        this.setFailure = this.setFailure.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
          [name]: value
        });
    }

    setSuccess() {
        this.setState({
            ...true,
            successEmailSent: true
        })
    }

    setFailure(wipeMessage) {
        this.setState({
            ...wipeMessage,
            failureEmail: wipeMessage
        })
    }

    returnHome() {
        window.location.href = '/'
    }

    sendEmail(event) {
        event.preventDefault();
        this.setFailure(false)
        const email = this.state.email
        fetch(LOGIN_HOST + '/v1/user/forgot', {
          method: 'POST',
          headers: {
           'Access-Control-Allow-Origin': '*',
           'Accept': 'application/json',
           'Content-Type': 'application/json',
           'App-Code': 'pokeme',
           'Alex-Login-Email': email
          }
        })
        .then((response) => {
            if (!response.ok) {
                this.setFailure(true)
            }
            else {
                this.setSuccess()
            }
            return response.json()
        })
        .then((data) => {
            console.log('Success:', data);
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    }

  render() {
      return (
      <div className="container">
          <div className="row login-wrapper">
            <div className="d-flex justify-content-center">
                <h1 class="text-primary"><b>Forgot Your Password?</b></h1>
            </div>
            <div className="d-flex justify-content-center">
                <h4 class="text-primary">Enter your email of your account. Check your email for a reset password link.</h4>
            </div>
          </div>
          {!this.state.successEmailSent &&
          <div>
              <form onSubmit={this.sendEmail}>
                  <div className="form-row">
                        <div className="form-group col">
                              <input id="emailpass" className="form-control" name="email" type="text" onChange={this.handleInputChange} />
                        </div>
                  </div>
                  <br />
                  <div className="form-row">
                    <button type="submit" className="btn btn-primary">Send Email</button>
                  </div>
              </form>
              <br />
              <div>
                  <button type="submit" className="btn btn-primary" onClick={() => {this.returnHome()}}>Back</button>
              </div>
          </div>
          }
          {this.state.successEmailSent &&
              <div className="row">
                  <h4 className="success">Success! Check your email for a password reset!</h4>
              </div>
          }
        {this.state.failureEmail &&
          <div className="row">
              <h4 className="error">Email failed to send sorry about that, try again later</h4>
          </div>
        }
      </div>
      )
  }

}
export default ForgotPassword;
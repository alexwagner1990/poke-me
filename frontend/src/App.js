import React, {Component} from "react";
import ReactDOM from "react-dom/client";
import {HOST, LOGIN_HOST} from "./utils/FetchUtils.js"
import { BrowserRouter, Switch, Route, Link } from "react-router-dom";
import logo from './logo.svg';
import './App.css';
import Layout from './pages/Layout.js'
import ForgotPassword from './pages/ForgotPassword.js'
import ResetPassword from './pages/ResetPassword.js'
import AccountSettings from './pages/AccountSettings.js'
import NotificationsList from './notifications/page/NotificationsList.js'
import DeleteNotification from './notifications/page/DeleteNotification.js'
import 'bootstrap/dist/css/bootstrap.min.css';
import LoggedInNav from "./components/LoggedInNav";
import NewNotification from './notifications/page/NewNotification.js'
import EditNotification from './notifications/page/EditNotification.js'
import Login from './login/pages/Login.js'
import FrequencyForm from './notifications/component/frequency/FrequencyForm.js'

class App extends Component {

   constructor(props) {
     super(props)
     this.state = {
         token: this.getToken()
     };
     this.setToken = this.setToken.bind(this);
     this.getToken = this.getToken.bind(this);
     this.removeToken = this.removeToken.bind(this);
     this.setUser = this.setUser.bind(this);
     this.login = this.login.bind(this);
   }

    componentDidMount() {
        this.setState({
            token: this.login(this.getToken())
        })
    }

   setToken(myToken) {
        this.setState({
            token: myToken
        })
        localStorage.setItem('poke-me-token', JSON.stringify(myToken))
   }

   removeToken() {
        this.setState({
            token: null
        })
        localStorage.removeItem('poke-me-token')
        localStorage.removeItem('poke-me-user')
   }

   getToken() {
        const tokenString = localStorage.getItem('poke-me-token');
        console.log(tokenString)
        return tokenString
   }

   setUser(userSet) {
        this.setState({
            ...userSet,
            user: userSet
        })
        localStorage.setItem('poke-me-user', JSON.stringify(userSet))
   }

   login(token) {
       if (!token) return;
       fetch(LOGIN_HOST + '/v1/token', {
           method: 'GET',
           headers: {
               'Content-Type': 'application/json',
               'Authorization': token,
               'App-Code': 'pokeme'
           }
       })
       .then((response) => response.json())
       .then((data) => {
           if (data.userId) {
               this.setToken(token.replaceAll('"',""))
           }
           else {
                console.error("INVALID TOKEN")
                localStorage.removeItem('poke-me-token')
           }
           return data
       })
       .then((token) => {
           if (token.token) {
               return fetch(LOGIN_HOST + '/v1/user', {
                   method: 'GET',
                   headers: {
                       'Content-Type': 'application/json',
                       'Authorization': token.token,
                       'App-Code': 'pokeme'
                   }
               })
           }
           throw Error("No Valid Token")
       })
       .then((response) => {
           return response.json()
       })
       .then((user) => {
           if (user) {
               this.setUser(user)
           }
       })
       .catch((error) => {
           console.error('Error:', error);
       });
   }

    render() {
        if (!this.state.token) {
            return (
                <div className="App">
                    <Switch>
                        <Route path="/forgotmypassword">
                            <ForgotPassword />
                        </Route>
                        <Route path="/resetpassword">
                            <ResetPassword />
                        </Route>
                        <Route path="/">
                            <Login setToken={this.setToken} setUser={this.setUser}/>
                        </Route>
                    </Switch>
                </div>
            )
        }
        return (
            <>
                <LoggedInNav setUser={this.setUser} removeToken={this.removeToken}/>
                <div className="App">
                    <Switch>
                        <Route path="/notifications">
                            <NotificationsList />
                        </Route>
                        <Route path="/delete/:id">
                            <DeleteNotification/>
                        </Route>
                        <Route path="/edit/:id">
                            <EditNotification/>
                        </Route>
                        <Route path="/newnotification">
                            <NewNotification/>
                        </Route>
                        <Route path="/frequency">
                            <FrequencyForm showSubmit={true}/>
                        </Route>
                        <Route path="/settings">
                            <AccountSettings />
                        </Route>
                        <Route path="/">
                            <Layout />
                        </Route>
                    </Switch>
                </div>
            </>
        );
    }
}

export default App;

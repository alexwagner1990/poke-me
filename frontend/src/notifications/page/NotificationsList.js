import React, {Component} from "react";
import {HOST} from "../../utils/FetchUtils.js"
import Table from '../../table/Table.js'

class NotificationList extends Component {

constructor(props) {
    super(props)
      const theadData = [
        {display: "Phone Numbers", key: "phoneNumbers"},
        {display: "Description", key: "description"},
        {display: "Text", key: "text"},
        {display: "Frequency", key: "frequencyDescription"},
        {display: "Media", key: "media"},
        {display: "Edit", key: "edit"},
        {display: "Delete", key: "delete"}
      ]
      this.state = {
          headers: theadData
      };

}
  componentDidMount() {
      fetch(HOST + '/api/notification', {
            method: 'GET',
            headers: {
               'Access-Control-Allow-Origin': '*',
               'Accept': 'application/json',
               'Authorization': localStorage.getItem('poke-me-token'),
               'App-Code': 'pokeme'
            }
          })
        .then((response) => {
            return response.json()
        })
        .then((data) => {
            this.setState({
                notifications: data
            });
        })
        .catch((error) => {
            console.error('Error:', error);
        });
  }

  render() {
    if (this.state.headers != null && this.state.notifications != null) {
        return (
            <div>
              <h2 class="text-primary"><b>Notifications</b></h2>
                <div className="App-intro">
                    <Table theadData={this.state.headers}
                        tbodyData={this.state.notifications}
                        customClass="table table-bordered table-striped"
                        customHeaderClass="table-light"/>
                </div>
            </div>
        );
    }
  }
}

export default NotificationList;
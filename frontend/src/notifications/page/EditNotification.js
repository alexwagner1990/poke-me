import React, {Component} from "react";
import {HOST} from "../../utils/FetchUtils.js"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams
} from "react-router-dom";
import { withRouter } from "react-router";
import Table from "../../table/Table"
import NotificationForm from "../component/NotificationForm"

class EditNotification extends Component {

    constructor(props) {
        super(props)
        const theadData = [
            {display: "Phone Numbers", key: "phoneNumbers"},
            {display: "Description", key: "description"},
            {display: "Text", key: "text"},
            {display: "Frequency", key: "frequencyDescription"},
            {display: "Media", key: "media"}
        ]
        this.state = {
          headers: theadData,
          errorMessage: "",
          successMessage: ""
        };
        this.setSuccess = this.setSuccess.bind(this)
        this.setFailure = this.setFailure.bind(this)
    }

    componentDidMount() {
      const id = this.props.match.params.id;
      fetch(HOST + '/api/notification/' + id, {
        method: "GET",
        headers: {
           'Access-Control-Allow-Origin': '*',
           'Accept': 'application/json',
           'Authorization': localStorage.getItem('poke-me-token'),
           'App-Code': 'pokeme'
        }
      })
        .then((response) => {
            if (!response.ok) {
                this.setState({
                    errorMessage: "Failed to retrieve notification"
                })
            }
            return response.json();
        })
        .then((data) => {
            this.setState({
                notifications: data
            });
        })
        .catch((err) => {
            console.log(err)
        })
    }

    setSuccess() {
        this.setState({
            successMessage: "Succesfully Edited Notification!"
        })
    }

    setFailure() {
        this.setState({
            errorMessage: "Failed to edit notification"
        })
    }

    submit(notification, finishLoading) {
        fetch(HOST + '/api/notification/', {
          method: 'PUT',
          headers: {
           'Access-Control-Allow-Origin': '*',
           'Accept': 'application/json',
           'Content-Type': 'application/json',
           'Authorization': localStorage.getItem('poke-me-token'),
           'App-Code': 'pokeme'
          },
          body: JSON.stringify(notification),
        })
        .then((response) => {
            if (!response.ok) {
                this.setFailure()
            }
            else {
                this.setSuccess()
            }
//            window.scrollTo(0, 0);
            return response.json()
        })
        .then((data) => {
            console.log('Success:', data);
            finishLoading()
        })
        .catch((error) => {
            console.error('Error:', error);
            finishLoading()
        });
    }

    render() {
        if (this.state.headers != null && this.state.notifications != null) {
            let notification = []
            notification.push(this.state.notifications)
            return (
                <div>
                  <h2 class="text-primary"><b>Edit This Notification</b></h2>
                    <div className="App-intro">
                        <Table theadData={this.state.headers} tbodyData={notification} customClass="table notification-table"/>
                        <hr/>
                        <NotificationForm setSuccess={this.setSuccess} setFailure={this.setFailure} submit={this.submit} notification={this.state.notifications}/>
                        <div>
                            {this.state.errorMessage &&
                                <h4 className="error">{this.state.errorMessage}</h4>
                            }
                            {this.state.successMessage &&
                                <h4 className="success">{this.state.successMessage}</h4>
                            }
                        </div>
                        <div className="bottom-spacing" />
                    </div>
                </div>
            );
        }
    }
}

export default withRouter(EditNotification);
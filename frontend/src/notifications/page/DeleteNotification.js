import React, {Component} from "react";
import {HOST} from "../../utils/FetchUtils.js"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams
} from "react-router-dom";
import { withRouter } from "react-router";
import Table from "../../table/Table.js";
import Button from 'react-bootstrap/Button';

class DeleteNotification extends Component {

    constructor(props) {
        super(props)
        const theadData = [
            {display: "Phone Numbers", key: "phoneNumbers"},
            {display: "Description", key: "description"},
            {display: "Text", key: "text"},
            {display: "Frequency", key: "frequencyDescription"},
            {display: "Media", key: "media"}
        ]
        this.state = {
          deleteId: props.match.params.id,
          headers: theadData,
          errorMessage: "",
          successMessage: "",
          isLoading: false
        };
        this.handleLoading = this.handleLoading.bind(this);
    }

    componentDidMount() {
      const id = this.props.match.params.id;
      this.setState({
        deleteId: id
      })
      fetch(HOST + '/api/notification/' + id, {
        method: 'GET',
        headers: {
           'Access-Control-Allow-Origin': '*',
           'Accept': 'application/json',
           'Authorization': localStorage.getItem('poke-me-token'),
           'App-Code': 'pokeme'
        }
      })
        .then((response) => {
            if (!response.ok) {
                this.setState({
                    errorMessage: "Failed to retrieve notification"
                })
            }
            return response.json();
        })
        .then((data) => {
            this.setState({
                notifications: data
            });
        })
        .catch((err) => {
            console.log(err)
        })
    }

    handleLoading() {
        this.setState({
            isLoading: !this.state.isLoading
        })
    }

    deleteNotificationConfirm(notification) {
        this.setState({
            errorMessage: "",
            successMessage: "",
            isLoading: true
        })
        fetch(HOST + '/api/notification/' + notification.id, {
          method: 'DELETE',
          headers: {
           'Access-Control-Allow-Origin': '*',
           'Accept': 'application/json',
           'Authorization': localStorage.getItem('poke-me-token'),
           'App-Code': 'pokeme'
          }
        })
        .then((response) => {
            if (!response.ok) {
                this.setState({
                    errorMessage: "Failed to delete notification"
                })
            }
            else {
                this.setState({
                    successMessage: "Successfully Deleted Notification!"
                })
            }
            return response.json()
        })
        .then((data) => {
            console.log('Success:', data);
            this.handleLoading()
        })
        .catch((error) => {
            console.error('Error:', error);
            this.handleLoading()
        });
    }

    render() {
        if (this.state.headers != null && this.state.notifications != null) {
            let notification = []
            notification.push(this.state.notifications)
            return (
                <div>
                  <h2 class="text-primary"><b>Delete Notification</b></h2>
                    <div className="App-intro">
                        <h4>Are you sure you want to delete this Notification?</h4>
                        <Table theadData={this.state.headers} tbodyData={notification} customClass="table notification-table"/>
                        <div className="form-group row">
                            <Button
                              variant="primary"
                              disabled={this.state.isLoading}
                              onClick={!this.state.isLoading ? () => this.deleteNotificationConfirm(notification[0]) : null}
                            >
                              {this.state.isLoading ? 'Loading…' : 'DELETE'}
                            </Button>
                        </div>
                        <div>
                            {this.state.errorMessage &&
                                <h4 className="error">{this.state.errorMessage}</h4>
                            }
                            {this.state.successMessage &&
                                <h4 className="success">{this.state.successMessage}</h4>
                            }
                        </div>
                        <div className="bottom-spacing" />
                    </div>
                </div>
            );
        }
    }
}

export default withRouter(DeleteNotification);
import React, {Component} from "react";
import {HOST} from "../../utils/FetchUtils.js"
import NotificationForm from "../component/NotificationForm";

class NewNotification extends Component {

    constructor(props) {
        super(props)
        this.state = {
            errorMessage: "",
            successMessage: ""
        }
        this.newNotificationSubmit = this.submit.bind(this);
        this.setFailure = this.setFailure.bind(this);
        this.setSuccess = this.setSuccess.bind(this);
    }

    setFailure() {
        this.setState({
            errorMessage: "Failed to create notification"
        })
    }

    setSuccess() {
        this.setState({
            successMessage: "Successfully Created Notification!"
        })
    }

    submit(notification, finishLoading) {
        this.setState({
            errorMessage: "",
            successMessage: ""
        })
        fetch(HOST + '/api/notification/', {
          method: 'POST',
          headers: {
           'Access-Control-Allow-Origin': '*',
           'Accept': 'application/json',
           'Content-Type': 'application/json',
           'Authorization': localStorage.getItem('poke-me-token'),
           'App-Code': 'pokeme'
          },
          body: JSON.stringify(notification),
        })
        .then((response) => {
            if (!response.ok) {
                this.setFailure()
            }
            else {
                this.setSuccess()
            }
//            window.scrollTo(0, 0);
            return response.json()
        })
        .then((data) => {
            console.log('Success:', data);
            finishLoading();
        })
        .catch((error) => {
            console.error('Error:', error);
            finishLoading();
        });
    }

    render() {
        return (
            <div>
              <h2 class="text-primary"><b>New Notification</b></h2>
                <div>
                    <NotificationForm setSuccess={this.setSuccess} setFailure={this.setFailure} submit={this.newNotificationSubmit}/>
                </div>
                <div>
                {this.state.errorMessage &&
                    <div>
                        <h4 className="error">{this.state.errorMessage}</h4>
                    </div>
                }
                {this.state.successMessage &&
                    <div>
                        <h4 className="success">{this.state.successMessage}</h4>
                    </div>
                }
                </div>
                <div className="bottom-spacing" />
            </div>
        );
    }
}

export default NewNotification;
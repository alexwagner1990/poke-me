import { Link } from "react-router-dom";
import React, { useEffect, useState } from "react";

const ImageItem = ({ image }) => {
    const [img, setImg] = useState();
    const myImageStyle = { width: '150px', height: '150px' };
    const fetchImage = async (imageUrl) => {
        const res = await fetch(imageUrl);
        const imageBlob = await res.blob();
        const imageObjectURL = URL.createObjectURL(imageBlob);
        setImg(imageObjectURL);
    };
      useEffect(() => {
        fetchImage(image);
      }, []);
    return (
        <img style={myImageStyle} src={img} alt="icon" />
    );
};

export default ImageItem;
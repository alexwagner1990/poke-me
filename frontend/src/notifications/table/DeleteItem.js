import { Link } from "react-router-dom";
import React, {Component} from "react";

const DeleteItem = ({ notification }) => {
    let deleteLink = "/delete/" + notification.id
    return (
        <td>
            <Link to={deleteLink}>Delete</Link>
        </td>
    );
};

export default DeleteItem;
import { Link } from "react-router-dom";
import React from "react";

const EditItem = ({ notification }) => {
    let editLink = "/edit/" + notification.id
    return (
        <td>
            <Link to={editLink}>Edit</Link>
        </td>
    );
};

export default EditItem;
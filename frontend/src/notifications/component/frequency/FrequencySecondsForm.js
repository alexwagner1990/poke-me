import React, { Component }  from 'react';
import {  Link } from "react-router-dom";
import {generateCheckboxNumbers, generateDropdownNumbers, buildArrayOfArrays} from "./FrequencyFormHelpers.js"

class FrequencySecondsForm extends Component {

    constructor(props) {
        super(props)
        this.state = {
            radio: "chooseMany",
            each: [],
            everyAt: '01',
            startingAt: '00'
        };
        this.handleSecondOptionChange = this.handleSecondOptionChange.bind(this);
        this.handleEveryAtOptionChange = this.handleEveryAtOptionChange.bind(this);
        this.handleStartAtOptionChange = this.handleStartAtOptionChange.bind(this);
    }

    componentDidMount() {
        this.setState({
            each: generateCheckboxNumbers(0, 59)
        })
    }

    handleSecondOptionChange(event) {
        console.log(event.target.value)
        this.setState({
            radio: event.target.value
        })
    }

    handleEveryAtOptionChange(event) {
        console.log(event.target.value)
        this.setState({
            everyAt: event.target.value
        })
    }

    handleStartAtOptionChange(event) {
        console.log(event.target.value)
        this.setState({
            startingAt: event.target.value
        })
    }

    handleSecondCheckboxChange(event) {
        console.log(event)
        const index = this.state.each.findIndex(item => item.name === event.name);
        let eachSecond = [...this.state.each]; // important to create a copy, otherwise you'll modify state outside of setState call
        eachSecond[index].value = !eachSecond[index].value;
        this.setState({each: eachSecond});
    }

    render () {
        let dropdownArray = generateDropdownNumbers(0, 59)
        let secondsRows = buildArrayOfArrays(generateCheckboxNumbers(0,59), 10)
        return (
        <>
            <form onSubmit={this.handleFreqSubmit}>
                <div className="form-group row">
                  <div className="col">
                    <input
                        id="radioSecondInput1"
                        type="radio"
                        value="every"
                        checked={this.state.radio == "every"}
                        onChange={this.handleSecondOptionChange}
                    />
                    <label for="radioSecondInput1" className="col-sm-2 col-form-label">Every Second</label>
                  </div>
                </div>
                <div className="form-group row">
                  <div className="col">
                    <input
                        id="radioSecondInput2"
                        type="radio"
                        value="at"
                        checked={this.state.radio == "at"}
                        onChange={this.handleSecondOptionChange}
                    />
                    <label for="radioSecondInput2" className="col-sm-2 col-form-label">Every
                        <select name="everyWhichSecond" value={this.state.everyAt} onChange={this.handleEveryAtOptionChange} disabled={!(this.state.radio == "at")}>
                            {dropdownArray.map((e, key) => {
                                return <option value={e}>{e}</option>;
                            })}
                        </select>
                        second(s) starting at second
                        <select name="everyWhichSecondStart" value={this.state.startingAt} onChange={this.handleStartAtOptionChange} disabled={!(this.state.radio == "at")}>
                            {dropdownArray.map((e, key) => {
                                return <option value={e}>{e}</option>;
                            })}
                        </select>
                    </label>
                  </div>
                </div>
                <div className="form-group row">
                  <div className="col">
                        <input type="radio"
                            id="radioSecondInput3"
                            value="chooseMany"
                            checked={this.state.radio == "chooseMany"}
                            onChange={this.handleSecondOptionChange}
                        />
                      <label for="radioSecondInput3" className="col-sm-2 col-form-label">
                        Specific Second (Choose Any)
                      </label>
                        <div name="specificSecond">
                            {secondsRows.map((secondsRow, skey) => {
                                return(
                                    <div className="form-group row">
                                        {secondsRow.map((e, key) => {
                                            return(
                                            <div className="col">
                                            <label htmlFor={key}>{e.name}</label>
                                            <input
                                                id={key}
                                                label={e.name}
                                                defaultChecked={e.name == '00'}
                                                disabled={!(this.state.radio == "chooseMany")}
                                                type="checkbox"
                                                value={e.name}
                                                onChange={() => this.handleSecondCheckboxChange(e)}
                                                />
                                            </div>
                                            )
                                        })}
                                    </div>
                                )
                            })}
                        </div>
                  </div>
                </div>
            </form>
        </>
        )
    }


}

export default FrequencySecondsForm
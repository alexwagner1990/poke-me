export function generateCheckboxNumbers(lowerNumber, upperNumber) {
    const options = []
    for (let i = lowerNumber; i <= upperNumber; i++) {
        let display = i < 10 ? "0" + i : i
        let obj = {}
        obj.name = display
        obj.value = (i == lowerNumber)
        options.push(obj)
    }
    return options
}

export function generateDropdownNumbers(lowerNumber, upperNumber) {
    const options = []
    for (let i = lowerNumber; i <= upperNumber; i++) {
        let display = i < 10 ? "0" + i : i
        options.push(display)
    }
    return options
}

export function generateCalendarDays() {
    const options = []
    for (let i = 1; i <= 31; i++) {
        let display = ""
        display += i
        if (i === 1 || i === 21 || i === 31) {
            display += "st"
        }
        else if (i === 2 || i === 22) {
            display += "nd"
        }
        else if (i === 3 || i === 23) {
            display += "rd"
        }
        else {
            display += "th"
        }
        options.push(display)
    }
    return options
}

export function generateMonths() {
    return ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"]
}

export function generateDays() {
    return ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
}

export function generateCheckboxes(array) {
    const options = []
    for (let i = 0; i < array.length; i++) {
        let display = array[i]
        let obj = {}
        obj.name = display
        obj.value = (i == 0)
        options.push(obj)
    }
    return options
}

export function buildArrayOfArrays(array, size) {
        let arrayOfArrays = []
        while (array.length > 0) {
            arrayOfArrays.push(array.splice(0, size))
        }
        return arrayOfArrays
}


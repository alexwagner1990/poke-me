import React, { Component }  from 'react';
import {  Link } from "react-router-dom";
import {generateCheckboxNumbers, generateDropdownNumbers, generateCheckboxes, generateDays, generateDayNumbers, generateCalendarDays, buildArrayOfArrays} from "./FrequencyFormHelpers.js"

class FrequencyDaysForm extends Component {

    constructor(props) {
        super(props)
        this.state = {
            radio: "every",
            each: [],
            each1: [],
            everyAt: '1',
            startingAt: 'Sunday',
            everyAt1: '1',
            startingAt1: '1st',
            dayNumberOfWeek: '1st',
            weekDay: 'Sunday',
            lastWeekDayOfMonth: 'Sunday'
        };
        this.handleDayOptionChange = this.handleDayOptionChange.bind(this);
        this.handleOptionChange = this.handleOptionChange.bind(this);
        this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
    }

    componentDidMount() {
        this.setState({
            each: generateCheckboxes(generateDays()),
            each1: generateCheckboxes(generateDropdownNumbers(1, 31)),
        })
    }

    handleDayOptionChange(event) {
        console.log(event.target.value)
        this.setState({
            radio: event.target.value
        })
    }

    handleOptionChange(event, stateValue) {
        console.log(event.target.value)
        this.setState({
            [stateValue]: event.target.value
        })
    }

    handleCheckboxChange(event, stateValue) {
        const index = this.state[stateValue].findIndex(item => item.name === event.name);
        let each = [...this.state[stateValue]]; // important to create a copy, otherwise you'll modify state outside of setState call
        each[index].value = !each[index].value;
        this.setState({[stateValue]: each});
    }

    render () {
        let dropdownArray = generateDropdownNumbers(1, 7)
        let dropdownArraySecond = generateDropdownNumbers(1,31)
        let shortNumbers = generateCalendarDays().slice(0,4)
        let daysRows = buildArrayOfArrays(generateCheckboxes(generateDays()), 7)
        let daysCalendarsRows = buildArrayOfArrays(generateCheckboxes(generateDropdownNumbers(1, 31)), 10)
        return (
        <>
            <form onSubmit={this.handleFreqSubmit}>
                    <div className="form-group row">
                      <div className="col">
                        <input
                            type="radio"
                            id="radioDayInput1"
                            value="every"
                            checked={this.state.radio == "every"}
                            onChange={this.handleDayOptionChange}
                        />
                        <label for="radioDayInput1" className="col-sm-2 col-form-label">Every Day</label>
                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col">
                        <input
                            type="radio"
                            id="radioDayInput2"
                            value="at"
                            checked={this.state.radio == "at"}
                            onChange={this.handleDayOptionChange}
                        />
                        <label for="radioSecondInput2" className="col-sm-2 col-form-label">Every
                            <select name="everyWhichDay" value={this.state.everyAt}  onChange={(e) => this.handleOptionChange(e, 'everyAt')}  disabled={!(this.state.radio == "at")}>
                                {dropdownArray.map((e, key) => {
                                    return <option value={e}>{e}</option>;
                                })}
                            </select>
                            day(s) starting on
                            <select name="everyWhichDayStart" value={this.state.startAt} onChange={(e) => this.handleOptionChange(e, 'startingAt')}  disabled={!(this.state.radio == "at")}>
                                {generateDays().map((e, key) => {
                                    return <option value={e}>{e}</option>;
                                })}
                            </select>
                        </label>
                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col">
                        <input
                            type="radio"
                            value="at1"
                            id="radioDayInput3"
                            checked={this.state.radio == "at1"}
                            onChange={this.handleDayOptionChange}
                        />
                        <label for="radioDayInput3" className="col-sm-2 col-form-label">Every
                            <select name="everyWhichDayMonth" value={this.state.everyAt1} onChange={(e) => this.handleOptionChange(e, 'everyAt1')}  disabled={!(this.state.radio == "at1")}>
                                {dropdownArraySecond.map((e, key) => {
                                    return <option value={e}>{e}</option>;
                                })}
                            </select>
                            day starting on the
                            <select name="everyWhichDayMonthStart" value={this.state.startAt1} onChange={(e) => this.handleOptionChange(e, 'startingAt1')}  disabled={!(this.state.radio == "at1")}>
                                {generateCalendarDays().map((e, key) => {
                                    return <option value={e}>{e}</option>;
                                })}
                            </select>
                            of the month
                        </label>
                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col">
                        <input type="radio"
                            value="chooseMany"
                            id="radioDayInput4"
                            checked={this.state.radio == "chooseMany"}
                            onChange={this.handleDayOptionChange}
                        />
                      <label for="radioDayInput4" className="col-sm-2 col-form-label">
                        Specific Day of the Week (Choose Any)
                      </label>
                        <div name="specificDayWeek">
                            {daysRows.map((daysRow, skey) => {
                                return (
                                    <div className="form-group row">
                                        {daysRow.map((e, key) => {
                                            return (<div className="col">
                                            <label htmlFor={key}>{e.name}</label>
                                            <input
                                                id={key}
                                                label={e.name}
                                                type="checkbox"
                                                value={e.name}
                                                disabled={!(this.state.radio == "chooseMany")}
                                                defaultChecked={e.name == 'SUN'}
                                                onChange={() => this.handleCheckboxChange(e, 'each')}
                                                />
                                            </div>)
                                        })}
                                    </div>
                                )
                            })}
                        </div>
                      </div>
                    </div>


                    <div className="form-group row">
                      <div className="col">
                        <input type="radio"
                            value="chooseMany1"
                            id="radioDayInput5"
                            checked={this.state.radio == "chooseMany1"}
                            onChange={this.handleDayOptionChange}
                        />
                      <label for="radioSecondInput5" className="col-sm-2 col-form-label">
                        Specific Day of the Month (Choose Any)
                      </label>
                        <div name="specificDayMonth">
                            {daysCalendarsRows.map((daysCalendarsRow, skey) => {
                                return (
                                    <div className="form-group row">
                                        {daysCalendarsRow.map((e, key) => {
                                            return <div className="col">
                                            <label htmlFor={key}>{e.name}</label>
                                            <input
                                                id={key}
                                                label={e.name}
                                                type="checkbox"
                                                value={e.name}
                                                defaultChecked={e.name == '01'}
                                                disabled={!(this.state.radio == "chooseMany1")}
                                                onChange={() => this.handleCheckboxChange(e, 'each1')}
                                                />
                                            </div>
                                        })}
                                    </div>
                                )
                            })}
                        </div>
                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col">
                        <input type="radio"
                            value="lastDayMonth"
                            id="radioDayInput5"
                            checked={this.state.radio == "lastDayMonth"}
                            onChange={this.handleDayOptionChange}
                        />
                        <label for="radioDayInput5" className="col-sm-2 col-form-label">Last Day of the Month</label>                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col">
                        <input type="radio"
                            value="firstWeekDayMonth"
                            id="radioDayInput6"
                            checked={this.state.radio == "firstWeekDayMonth"}
                            onChange={this.handleDayOptionChange}
                        />
                        <label for="radioDayInput6" className="col-sm-2 col-form-label">First Weekday of the Month</label>
                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col">
                        <input type="radio"
                            value="lastWeekDayMonth"
                            id="radioDayInput7"
                            checked={this.state.radio == "lastWeekDayMonth"}
                            onChange={this.handleDayOptionChange}
                        />
                        <label for="radioDayInput7" className="col-sm-2 col-form-label">Last Weekday of the Month</label>
                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col">
                        <input type="radio"
                            value="dayWeekMonth"
                            id="radioDayInput7"
                            checked={this.state.radio == "dayWeekMonth"}
                            onChange={this.handleDayOptionChange}
                        />
                        <label for="radioSecondInput7" className="col-sm-2 col-form-label">On the
                            <select name="dayNumberOfWeek" value={this.state.dayNumberOfWeek} onChange={(e) => this.handleOptionChange(e, 'dayNumberOfWeek')} disabled={!(this.state.radio == "dayWeekMonth")}>
                                {shortNumbers.map((e, key) => {
                                    return <option value={e}>{e}</option>;
                                })}
                            </select>
                            <select name="weekDay" value={this.state.weekDay} onChange={(e) => this.handleOptionChange(e, 'weekDay')} disabled={!(this.state.radio == "dayWeekMonth")}>
                                {generateDays().map((e, key) => {
                                    return <option value={e}>{e}</option>;
                                })}
                            </select>
                            of the month
                        </label>
                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col">
                        <input type="radio"
                            value="weekdayOfMonthLast"
                            id="radioDayInput8"
                            checked={this.state.radio == "weekdayOfMonthLast"}
                            onChange={this.handleDayOptionChange}
                        />
                        <label for="radioDayInput8" className="col-sm-2 col-form-label">Last
                            <select name="lastWeekDayOfMonth" value={this.state.lastWeekDayOfMonth} onChange={(e) => this.handleOptionChange(e, 'lastWeekDayOfMonth')} disabled={!(this.state.radio == "weekdayOfMonthLast")}>
                                {generateDays().map((e, key) => {
                                    return <option value={e}>{e}</option>;
                                })}
                            </select>
                            of the Month
                        </label>
                      </div>
                    </div>
            </form>
        </>
        )
    }

}

export default FrequencyDaysForm
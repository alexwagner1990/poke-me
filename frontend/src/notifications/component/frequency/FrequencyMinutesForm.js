import React, { Component }  from 'react';
import {  Link } from "react-router-dom";
import {generateCheckboxNumbers, generateDropdownNumbers, buildArrayOfArrays} from "./FrequencyFormHelpers.js"

class FrequencyMinutesForm extends Component {

    constructor(props) {
        super(props)
        this.state = {
            radio: "every",
            each: [],
            everyAt: '01',
            startingAt: '00'
        };
        this.handleMinuteOptionChange = this.handleMinuteOptionChange.bind(this);
        this.handleEveryAtOptionChange = this.handleEveryAtOptionChange.bind(this);
        this.handleStartAtOptionChange = this.handleStartAtOptionChange.bind(this);
    }

    componentDidMount() {
        this.setState({
            each: generateCheckboxNumbers(0, 59)
        })
    }

    handleMinuteOptionChange(event) {
        console.log(event.target.value)
        this.setState({
            radio: event.target.value
        })
    }

    handleMinuteCheckboxChange(event) {
        console.log(event)
        const index = this.state.each.findIndex(item => item.name === event.name);
        let each = [...this.state.each]; // important to create a copy, otherwise you'll modify state outside of setState call
        each[index].value = !each[index].value;
        this.setState({each: each}, () => console.log(this.state.each));
    }

    handleEveryAtOptionChange(event) {
        console.log(event.target.value)
        this.setState({
            everyAt: event.target.value
        })
    }

    handleStartAtOptionChange(event) {
        console.log(event.target.value)
        this.setState({
            startingAt: event.target.value
        })
    }

    render () {
        let dropdownArray = generateDropdownNumbers(0, 59)
        let minutesRows = buildArrayOfArrays(generateCheckboxNumbers(0, 59), 10)
        return (
        <>
            <form onSubmit={this.handleFreqSubmit}>
                <div className="form-group row">
                  <div className="col">
                    <input
                        type="radio"
                        id="radioMinuteInput1"
                        value="every"
                        checked={this.state.radio == "every"}
                        onChange={this.handleMinuteOptionChange}
                    />
                    <label for="radioMinuteInput1" className="col-sm-2 col-form-label">Every Minute</label>
                  </div>
                </div>
                <div className="form-group row">
                  <div className="col">
                        <input
                            type="radio"
                            id="radioMinuteInput2"
                            value="at"
                            checked={this.state.radio == "at"}
                            onChange={this.handleMinuteOptionChange}
                        />
                        <label for="radioMinuteInput2" className="col-sm-2 col-form-label">Every
                        <select name="everyWhichMinute" value={this.state.everyAt} onChange={this.handleEveryAtOptionChange} disabled={!(this.state.radio == "at")}>
                            {dropdownArray.map((e, key) => {
                                return <option value={e}>{e}</option>;
                            })}
                        </select>
                        minute(s) starting at minute
                        <select name="everyWhichMinuteStart" value={this.state.startingAt} onChange={this.handleStartAtOptionChange} disabled={!(this.state.radio == "at")}>
                            {dropdownArray.map((e, key) => {
                                return <option value={e}>{e}</option>;
                            })}
                        </select>
                      </label>
                  </div>
                </div>
                  <div className="form-group row">
                    <div className="col">
                        <input type="radio"
                            id="radioMinuteInput3"
                            value="chooseMany"
                            checked={this.state.radio == "chooseMany"}
                            onChange={this.handleMinuteOptionChange}
                        />
                        <label for="radioSecondInput3" className="col-sm-2 col-form-label">
                          Specific Minute (Choose Any)
                        </label>
                        <div name="specificMinute">
                            {minutesRows.map((minutesRow, skey) => {
                                return(
                                    <div className="form-group row">
                                        {minutesRow.map((e, key) => {
                                            return (
                                            <div className="col">
                                            <label htmlFor={key}>{e.name}</label>
                                            <input
                                                id={key}
                                                label={e.name}
                                                type="checkbox"
                                                value={e.name}
                                                disabled={!(this.state.radio == "chooseMany")}
                                                defaultChecked={e.name == '00'}
                                                onChange={() => this.handleMinuteCheckboxChange(e)}
                                                />
                                            </div>
                                            )
                                        })}
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                  </div>
            </form>
        </>
        )
    }
}

export default FrequencyMinutesForm
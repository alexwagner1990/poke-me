import React, { Component }  from 'react';
import {  Link } from "react-router-dom";
import {Tabs, Tab, Modal, Row, Button, Col, Form, Card, Container} from "react-bootstrap";
import FrequencySecondsForm from "./FrequencySecondsForm.js"
import FrequencyHoursForm from "./FrequencyHoursForm.js"
import FrequencyMinutesForm from "./FrequencyMinutesForm.js"
import FrequencyYearsForm from "./FrequencyYearsForm.js"
import FrequencyMonthsForm from "./FrequencyMonthsForm.js"
import FrequencyDaysForm from "./FrequencyDaysForm.js"
import {buildCron} from "./BuildCronStatement.js"
import "./Frequency.css"

class FrequencyForm extends Component {

    constructor(props) {
        super(props)
        this.state = {
            freqPage: "seconds",
            showSubmit: props.showSubmit
        };
        this.showFreq = this.showFreq.bind(this);
        this.buildCronStatement = this.buildCronStatement.bind(this);
        this.seconds = React.createRef();
        this.minutes = React.createRef();
        this.hours = React.createRef();
        this.months = React.createRef();
        this.days = React.createRef();
        this.years = React.createRef();
    }

    showFreq(showWhich) {
        this.setState(prevState => {
            return {
                ...prevState,
                freqPage: showWhich
            }
        })
    }

    buildCronStatement() {
        return buildCron(this.seconds.current.state,
            this.minutes.current.state,
            this.hours.current.state,
            this.days.current.state,
            this.months.current.state,
            this.years.current.state)
    }

    render () {
        return (
        <>
            <div className="container">
             <Container>
                 <Row>
                     <Col>
                         <Tabs defaultActiveKey="Seconds"
                               id="controlled-tab-example">
                             <Tab eventKey="Seconds" title="Seconds">
                                 <FrequencySecondsForm ref={this.seconds} />
                             </Tab>
                             <Tab eventKey="Minutes" title="Minutes">
                                 <FrequencyMinutesForm ref={this.minutes} />
                             </Tab>
                             <Tab eventKey="Hours" title="Hours">
                                 <FrequencyHoursForm ref={this.hours} />
                             </Tab>
                             <Tab eventKey="Days" title="Days">
                                 <FrequencyDaysForm ref={this.days} />
                             </Tab>
                             <Tab eventKey="Months" title="Months">
                                 <FrequencyMonthsForm ref={this.months} />
                             </Tab>
                             <Tab eventKey="Years" title="Years">
                                 <FrequencyYearsForm ref={this.years} />
                             </Tab>
                         </Tabs>
                     </Col>
                 </Row>
             </Container>
             {this.state.showSubmit &&
                <div className="freq-submit-button">
                     <button onClick={this.buildCronStatement}>
                         Submit All
                     </button>
                 </div>
             }
            </div>
        </>
        )
    }
}

export default FrequencyForm
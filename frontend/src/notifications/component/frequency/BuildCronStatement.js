export function buildCron(seconds, minutes, hours, days, months, years) {
    let cronParts = []
    let daysPart = buildPortion(days)
    let dayOfMonthOrWeek = putQuestionOnDayOfMonth(days.radio)
    cronParts.push(buildPortion(seconds))
    cronParts.push(buildPortion(minutes))
    cronParts.push(buildPortion(hours))
    if (dayOfMonthOrWeek) {
        cronParts.push("?")
    }
    else {
        cronParts.push(daysPart)
    }
    cronParts.push(buildPortion(months))
    if (dayOfMonthOrWeek) {
        cronParts.push(daysPart)
    }
    else {
        cronParts.push("?")
    }
    cronParts.push(buildPortion(years))
    let final = cronParts.join(" ")
    return final
}

export function buildPortion(state) {
    let display = ""
    switch(state.radio) {
        case "every":
        case "every1":
            display = "*"
            break;
        case "at":
            display = everyStartingAt(state.everyAt, state.startingAt)
            break;
        case "at1":
            display = everyStartingAt(state.everyAt1, state.startingAt1)
            break;
        case "chooseMany":
            display = specific(state.each);
            break;
        case "chooseMany1":
            display = specific(state.each1);
            break;
        case "lastDayMonth":
            display ="L"
            break;
        case "firstWeekDayMonth":
            display = "1W"
            break;
        case "lastWeekDayMonth":
            display = "LW"
            break;
        case "dayWeekMonth":
            display = numberOfWeek(state.dayNumberOfWeek, state.weekDay)
            break;
        case "weekdayOfMonthLast":
            display = (convertThingToNumber(state.lastWeekDayOfMonth) + "L")
            break;
    }
    return display
}

export function everyStartingAt(every, startingAt) {
    return convertThingToNumber(startingAt) + "/" + convertThingToNumber(every)
}

export function numberOfWeek(number, weekday) {
    return convertThingToNumber(weekday) + "#" + convertThingToNumber(number)
}

function convertToNumber(num) {
    let fir = Array.from(num)[0];
    return fir === '0' ? Array.from(num)[1] : num
}

function convertThingToNumber(mon) {
    const months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"]
    const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
    if (months.includes(mon)) {
        return (months.indexOf(mon) + 1)
    }
    if (days.includes(mon)) {
        return (days.indexOf(mon) + 1)
    }
    if (mon.endsWith("st") || mon.endsWith("nd") || mon.endsWith("rd") || mon.endsWith("th")) {
        return mon.slice(0, -2)
    }
    return convertToNumber(mon)
}

export function specific(seconds) {
    const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
    let display = ""
    for (let i = 0; i < seconds.length; i++) {
        if (seconds[i].value) {
            if (days.includes(seconds[i].name)) {
                display += convertToDay(i)
            }
            else {
                display += convertToNumber(seconds[i].name)
            }
            display += ","
        }
    }
    display = display.slice(0, -1)
    return display
}

export function convertToDay(dayNum) {
    const days = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"]
    return days[dayNum]
}

export function putQuestionOnDayOfMonth(state) {
    return (
        state === "every"
        || state === "every1"
        || state === "at"
        || state === "chooseMany"
        || state === "dayWeekMonth"
        || state === "weekdayOfMonthLast"
        )
}
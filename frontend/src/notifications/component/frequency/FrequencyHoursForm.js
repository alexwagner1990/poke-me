import React, { Component }  from 'react';
import {  Link } from "react-router-dom";
import {generateCheckboxNumbers, generateDropdownNumbers, buildArrayOfArrays} from "./FrequencyFormHelpers.js"

class FrequencyHoursForm extends Component {

    constructor(props) {
        super(props)
        this.state = {
            radio: "every",
            each: [],
            everyAt: '01',
            startingAt: '00'
        };
        this.handleHourOptionChange = this.handleHourOptionChange.bind(this);
        this.handleEveryAtOptionChange = this.handleEveryAtOptionChange.bind(this);
        this.handleStartAtOptionChange = this.handleStartAtOptionChange.bind(this);
    }

    componentDidMount() {
        this.setState({
            each: generateCheckboxNumbers(0, 23)
        })
    }

    handleHourOptionChange(event) {
        console.log(event.target.value)
        this.setState({
            radio: event.target.value
        })
    }

    handleHourCheckboxChange(event) {
        console.log(event)
        const index = this.state.each.findIndex(item => item.name === event.name);
        let each = [...this.state.each]; // important to create a copy, otherwise you'll modify state outside of setState call
        each[index].value = !each[index].value;
        this.setState({each: each});
    }

    handleEveryAtOptionChange(event) {
        console.log(event.target.value)
        this.setState({
            everyAt: event.target.value
        })
    }

    handleStartAtOptionChange(event) {
        console.log(event.target.value)
        this.setState({
            startingAt: event.target.value
        })
    }

    render () {
        let dropdownArray = generateDropdownNumbers(0, 23)
        let hoursRows = buildArrayOfArrays(generateCheckboxNumbers(0, 23), 6)
        return (
        <>
            <form onSubmit={this.handleFreqSubmit}>
                    <div className="form-group row">
                      <div className="col">
                        <input
                            type="radio"
                            id="radioHourInput1"
                            value="every"
                            checked={this.state.radio == "every"}
                            onChange={this.handleHourOptionChange}
                        />
                        <label for="radioHourInput1" className="col-sm-2 col-form-label">Every Hour</label>
                      </div>
                    </div>
                    <div className="form-group row">
                      <div className="col">
                        <input
                            type="radio"
                            id="radioHourInput2"
                            value="at"
                            checked={this.state.radio == "at"}
                            onChange={this.handleHourOptionChange}
                        />
                          <label for="radioHourInput2" className="col-sm-2 col-form-label">Every
                            <select name="everyWhichHour" value={this.state.everyWhichHour} onChange={this.handleEveryAtOptionChange} disabled={!(this.state.radio == "at")}>
                                {dropdownArray.map((e, key) => {
                                    return <option value={e}>{e}</option>;
                                })}
                            </select>
                            hour(s) starting at hour
                            <select name="everyWhichHourStart" value={this.state.everyWhichHourStart} onChange={this.handleStartAtOptionChange} disabled={!(this.state.radio == "at")}>
                                {dropdownArray.map((e, key) => {
                                    return <option value={e}>{e}</option>;
                                })}
                            </select>
                          </label>
                      </div>
                    </div>
                    <div className="form-group row">
                      <div className="col">
                        <input type="radio"
                            value="chooseMany"
                            id="radioHourInput3"
                            checked={this.state.radio == "chooseMany"}
                            onChange={this.handleHourOptionChange}
                        />
                        <label for="radioHourInput3" className="col-sm-2 col-form-label">
                          Specific Hour (Choose Any)
                        </label>
                        <div name="specificHour">
                          {hoursRows.map((hoursRow, skey) => {
                            return (
                              <div className="form-group row">
                                {hoursRow.map((e, key) => {
                                    return (
                                    <div className="col">
                                    <label htmlFor={key}>{e.name}</label>
                                    <input
                                        id={key}
                                        label={e.name}
                                        type="checkbox"
                                        value={e.name}
                                        disabled={!(this.state.radio == "chooseMany")}
                                        defaultChecked={e.name == '00'}
                                        onChange={() => this.handleHourCheckboxChange(e)}
                                        />
                                    </div>
                                    )
                                })}
                              </div>
                            )
                          })}
                        </div>
                      </div>
                    </div>
            </form>
        </>
        )
    }
}

export default FrequencyHoursForm
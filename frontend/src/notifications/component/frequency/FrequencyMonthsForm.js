import React, { Component }  from 'react';
import {  Link } from "react-router-dom";
import {generateCheckboxNumbers, generateDropdownNumbers, generateCheckboxes, generateMonths, buildArrayOfArrays} from "./FrequencyFormHelpers.js"

class FrequencyMonthsForm extends Component {

    constructor(props) {
        super(props)
        this.state = {
            radio: "every",
            each: [],
            everyAt: '01',
            startingAt: 'JAN'
        };
        this.handleMonthOptionChange = this.handleMonthOptionChange.bind(this);
        this.handleEveryAtOptionChange = this.handleEveryAtOptionChange.bind(this);
        this.handleStartAtOptionChange = this.handleStartAtOptionChange.bind(this);
    }

    componentDidMount() {
        this.setState({
            each: generateCheckboxes(generateMonths())
        })
    }

    handleMonthOptionChange(event) {
        console.log(event.target.value)
        this.setState({
            radio: event.target.value
        })
    }

    handleMonthCheckboxChange(event) {
        console.log(event)
        const index = this.state.each.findIndex(item => item.name === event.name);
        let each = [...this.state.each]; // important to create a copy, otherwise you'll modify state outside of setState call
        each[index].value = !each[index].value;
        this.setState({each: each});
    }

    handleEveryAtOptionChange(event) {
        console.log(event.target.value)
        this.setState({
            everyAt: event.target.value
        })
    }

    handleStartAtOptionChange(event) {
        console.log(event.target.value)
        this.setState({
            startingAt: event.target.value
        })
    }

    render () {
        let dropdownArray = generateDropdownNumbers(1, 12)
        let monthsRows = buildArrayOfArrays(generateCheckboxes(generateMonths()), 4)
        return (
        <>
            <form onSubmit={this.handleFreqSubmit}>
                    <div className="form-group row">
                      <div className="col">
                        <input
                            type="radio"
                            id="radioMonthInput1"
                            value="every"
                            checked={this.state.radio == "every"}
                            onChange={this.handleMonthOptionChange}
                        />
                      <label for="radioMonthInput1" className="col-sm-2 col-form-label">Every Month</label>
                      </div>
                    </div>
                    <div className="form-group row">
                      <div className="col">
                        <input
                            type="radio"
                            id="radioMonthInput2"
                            value="at"
                            checked={this.state.radio == "at"}
                            onChange={this.handleMonthOptionChange}
                        />
                        <label for="radioMonthInput2" className="col-sm-2 col-form-label">Every
                            <select name="everyWhichMonth" value={this.state.everyAt} onChange={this.handleEveryAtOptionChange} disabled={!(this.state.radio == "at")}>
                                {dropdownArray.map((e, key) => {
                                    return <option value={e}>{e}</option>;
                                })}
                            </select>
                            month(s) starting at month
                            <select name="everyWhichMonthStart" value={this.state.startingAt} onChange={this.handleStartAtOptionChange} disabled={!(this.state.radio == "at")}>
                                {generateMonths().map((e, key) => {
                                    return <option value={e}>{e}</option>;
                                })}
                            </select>
                        </label>
                      </div>
                    </div>
                    <div className="form-group row">
                      <div className="col">
                        <input type="radio"
                            id="radioMonthInput3"
                            value="chooseMany"
                            checked={this.state.radio == "chooseMany"}
                            onChange={this.handleMonthOptionChange}
                        />
                          <label for="radioMonthInput3" className="col-sm-2 col-form-label">
                            Specific Month (Choose Any)
                          </label>
                        <div name="specificMonth">
                            {monthsRows.map((monthsRow, skey) => {
                                return (
                                    <div className="form-group row">
                                        {monthsRow.map((e, key) => {
                                            return <div className="col">
                                            <label htmlFor={key}>{e.name}</label>
                                            <input
                                                id={key}
                                                label={e.name}
                                                type="checkbox"
                                                disabled={!(this.state.radio == "chooseMany")}
                                                value={e.name}
                                                defaultChecked={e.name == '00'}
                                                onChange={() => this.handleMonthCheckboxChange(e)}
                                                />
                                            </div>
                                        })}
                                    </div>
                                )
                            })}
                        </div>
                      </div>
                    </div>
            </form>
        </>
        )
    }


}

export default FrequencyMonthsForm
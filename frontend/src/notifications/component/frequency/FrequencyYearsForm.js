import React, { Component }  from 'react';
import {  Link } from "react-router-dom";
import {generateCheckboxNumbers, generateDropdownNumbers, buildArrayOfArrays} from "./FrequencyFormHelpers.js"

class FrequencyYearsForm extends Component {

    constructor(props) {
        super(props)
        this.state = {
            radio: "every",
            each: [],
            everyAt: '01',
            startingAt: '2023'
        };
        this.handleYearOptionChange = this.handleYearOptionChange.bind(this);
        this.handleEveryAtOptionChange = this.handleEveryAtOptionChange.bind(this);
        this.handleStartAtOptionChange = this.handleStartAtOptionChange.bind(this);
    }

    componentDidMount() {
        this.setState({
            each: generateCheckboxNumbers(2023, 2062)
        })
    }

    handleYearOptionChange(event) {
        console.log(event.target.value)
        this.setState({
            radio: event.target.value
        })
    }

    handleYearCheckboxChange(event) {
        console.log(event)
        const index = this.state.each.findIndex(item => item.name === event.name);
        let each = [...this.state.each]; // important to create a copy, otherwise you'll modify state outside of setState call
        each[index].value = !each[index].value;
        this.setState({each: each});
    }

    handleEveryAtOptionChange(event) {
        console.log(event.target.value)
        this.setState({
            everyAt: event.target.value
        })
    }

    handleStartAtOptionChange(event) {
        console.log(event.target.value)
        this.setState({
            startingAt: event.target.value
        })
    }

    render () {
        let dropdownArray = generateDropdownNumbers(1, 100)
        let otherDropdownArray = generateDropdownNumbers(2023, 2062)
        let yearsRows = buildArrayOfArrays(generateCheckboxNumbers(2023, 2062), 10)
        return (
        <>
            <form onSubmit={this.handleFreqSubmit}>
                    <div className="form-group row">
                      <div className="col">
                        <input
                            type="radio"
                            id="radioYearInput1"
                            value="every"
                            checked={this.state.radio == "every"}
                            onChange={this.handleYearOptionChange}
                        />
                        <label for="radioYearInput1" className="col-sm-2 col-form-label">Every Year</label>
                      </div>
                    </div>
                    <div className="form-group row">
                      <div className="col">
                        <input
                            type="radio"
                            id="radioYearSecondInput2"
                            value="at"
                            checked={this.state.radio == "at"}
                            onChange={this.handleYearOptionChange}
                        />
                        <label for="radioSecondInput2" className="col-sm-2 col-form-label">Every
                            <select name="everyWhichYear" value={this.state.everyWhichYear} onChange={this.handleEveryAtOptionChange} disabled={!(this.state.radio == "at")}>
                                {dropdownArray.map((e, key) => {
                                    return <option value={e}>{e}</option>;
                                })}
                            </select>
                            year(s) starting at year
                            <select name="everyWhichYearStart" value={this.state.everyWhichYearStart} onChange={this.handleStartAtOptionChange} disabled={!(this.state.radio == "at")}>
                                {otherDropdownArray.map((e, key) => {
                                    return <option value={e}>{e}</option>;
                                })}
                            </select>
                        </label>
                      </div>
                    </div>
                    <div className="form-group row">
                      <div className="col">
                        <input type="radio"
                            value="chooseMany"
                            id="radioYearInput3"
                            checked={this.state.radio == "chooseMany"}
                            onChange={this.handleYearOptionChange}
                        />
                        <label for="radioYearInput3" className="col-sm-2 col-form-label">
                            Specific Year (Choose Any)
                        </label>
                        <div name="specificYear">
                            {yearsRows.map((yearsRow, skey) => {
                                return (
                                    <div className="form-group row">
                                        {yearsRow.map((e, key) => {
                                            return <div className="col">
                                            <label htmlFor={key}>{e.name}</label>
                                            <input
                                                id={key}
                                                label={e.name}
                                                type="checkbox"
                                                disabled={!(this.state.radio == "chooseMany")}
                                                value={e.name}
                                                defaultChecked={e.name == '00'}
                                                onChange={(e) => this.handleYearCheckboxChange(e)}
                                                />
                                            </div>
                                        })}
                                    </div>
                                )
                            })}
                        </div>
                      </div>
                    </div>
            </form>
        </>
        )
    }
}

export default FrequencyYearsForm
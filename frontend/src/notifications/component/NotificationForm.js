import { Link } from "react-router-dom";
import React, {Component} from "react";
import FrequencyForm from "./frequency/FrequencyForm.js"
import Button from 'react-bootstrap/Button';
import "../Notification.css"

class NotificationForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
        id: props.notification ? props.notification.id : null,
        phoneNumber: props.notification ? props.notification.phoneNumbers.join() : '',
        description: props.notification ? props.notification.description : '',
        text: props.notification ? props.notification.text : '',
        frequency: props.notification ? props.notification.frequency : '',
        media: props.notification ? props.notification.media : '',
        instances: props.notification ? (props.notification.instances ? props.notification.instances : 'false' ) : 'false',
        cronExpression: false,
        isLoading: false
    };
    this.freqForm = React.createRef();
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleLoading = this.handleLoading.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  handleCheckboxChange(event) {
    const target = event.target;
    const value = target.checked;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  handleLoading() {
    this.setState({
        isLoading: !this.state.isLoading
    })
  }

  handleSubmit(event) {
    this.handleLoading()
    event.preventDefault();
    let freq = (this.state.cronExpression ?
        this.state.frequency : this.freqForm.current.buildCronStatement())
    const notification = {
        id: this.state.id ? this.state.id : null,
        phoneNumbers: this.state.phoneNumber.split(','),
        description: this.state.description,
        text: this.state.text,
        frequency: freq,
        media: this.state.media,
        instances: this.state.instances
    }
    this.props.submit(notification, this.handleLoading);
  }

  render() {
    return (
    <div className="container">
      <form onSubmit={this.handleSubmit}>
        <div className="form-group row">
          <div className="col-sm-10">
            <label for="phoneNumberInput" className="col-sm-2 col-form-label">Phone Number</label>
            <input name="phoneNumber" id="phoneNumberInput" type="text" value={this.state.phoneNumber} onChange={this.handleInputChange} />
          </div>
        </div>
        <div className="form-group row">
          <div className="col-sm-10">
            <label for="descriptionInput" className="col-sm-2 col-form-label">Description</label>
            <textarea id="descriptionInput" name="description" value={this.state.description} onChange={this.handleInputChange} />
          </div>
        </div>
        <div className="form-group row">
          <div className="col-sm-10">
            <label for="textMessageInput" className="col-sm-2 col-form-label">Text of Message</label>
            <textarea id="textMessageInput" name="text" value={this.state.text} onChange={this.handleInputChange} />
          </div>
        </div>
        <div className="form-group row">
          <div className="col-sm-10">
            <label for="mediaInput" className="col-sm-2 col-form-label">Media (URL)</label>
            <input id="mediaInput" name="media" type="text" value={this.state.media} onChange={this.handleInputChange} />
          </div>
        </div>
        <div className="form-group row">
          <div className="col-sm-10">
            <label for="singleMessageInput" className="col-sm-6 col-form-label">Single Message (Send only on next Frequency)</label>
            <input id="singleMessageInput" name="instances" type="checkbox" value={this.state.instances} onChange={this.handleCheckboxChange} />
          </div>
        </div>
        <div className="form-group row">
          <div className="col-sm-10">
            <label for="cronExpressionInput" className="col-sm-6 col-form-label">Advanced Frequency Editor</label>
            <input id="cronExpressionInput" name="cronExpression" type="checkbox" value={this.state.cronExpression} onChange={this.handleCheckboxChange} />
          </div>
        </div>
        <div className="form-group row">
        {!this.state.cronExpression &&
        <label>
          <b>Frequency</b>
          <FrequencyForm showSubmit={false} ref={this.freqForm}/>
        </label>
        }
        {this.state.cronExpression &&
        <div className="col-sm-10">
          <label for="frequency" className="col-sm-2 col-form-label"><b>Cron Expression</b></label>
          <input name="frequency" type="text" value={this.state.frequency} onChange={this.handleInputChange} />
        </div>
        }
        </div>
        <div className="form-group row">
            <Button
              variant="primary"
              disabled={this.state.isLoading}
              onClick={!this.state.isLoading ? this.handleSubmit : null}
            >
              {this.state.isLoading ? 'Loading…' : 'Submit'}
            </Button>
        </div>
      </form>
    </div>
    );
  }
}

export default NotificationForm;
import React from "react";

const TableItem = ({ data }) => {
    return (
        <td>
            {data}
        </td>
    );
};

export default TableItem;
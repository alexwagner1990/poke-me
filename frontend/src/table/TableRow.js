import React from "react";
import TableItem from "./TableItem.js"
import DeleteItem from "../notifications/table/DeleteItem.js"
import EditItem from "../notifications/table/EditItem.js"
import ImageItem from "../notifications/table/ImageItem.js"

const TableRow = ({ key, headers, data }) => {
    return (
        <tr>
            {headers.map((item) => {
                if (item.key === 'edit') {
                    return <EditItem notification={data} />
                }
                if (item.key === 'delete') {
                    return <DeleteItem notification={data} />
                }
                if (item.key === 'media' && data[item.key] && data[item.key].includes("http")) {
                    return <ImageItem image={data[item.key]} />
                }
                return <TableItem key={item} data={data[item.key]}/>;
            })}
        </tr>
    );
};

export default TableRow;
import React from "react";
import TableRow from "./TableRow.js";
import TableHeader from "./TableHeader.js";

const Table = ({ theadData, tbodyData, customClass, customHeaderClass }) => {

    return (
        <div className="table-responsive">
            <table className={customClass}>
                <TableHeader headers={theadData} customClass={customHeaderClass} />
                <tbody>
                    {tbodyData.map((item, key) => {
                        return <TableRow key={key} headers={theadData} data={item} />;
                    })}
                </tbody>
            </table>
        </div>
    );
};

export default Table;
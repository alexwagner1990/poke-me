import React from "react";

const TableHeadItem = ({ key, display }) => {
    return (
        <th key={key} title={key}>
            {display}
        </th>
    );
};

export default TableHeadItem;
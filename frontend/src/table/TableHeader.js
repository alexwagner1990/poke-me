import React from "react";
import TableHeadItem from "./TableHeadItem.js"

const TableHeader = ({ headers, customClass }) => {
    return (
        <thead className={customClass}>
            <tr>
                {headers.map((h) => {
                    return <TableHeadItem key={h.key} display={h.display} />;
                })}
            </tr>
        </thead>
    );
};

export default TableHeader;
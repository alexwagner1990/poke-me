package com.pokeme.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigProps {

    private Properties props;

    public ConfigProps() {
        Properties prop = new Properties();
        try (InputStream fis = getFileFromResourceAsStream("properties.config")) {
            prop.load(fis);
        } catch (FileNotFoundException ex) {
            throw new RuntimeException("Error Reading File", ex);
        } catch (IOException ex) {
            throw new RuntimeException("Error Printing File", ex);
        }
        this.props = prop;
    }

    private InputStream getFileFromResourceAsStream(String fileName) {
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream(fileName);
        if (inputStream == null) {
            throw new IllegalArgumentException("file not found! " + fileName);
        } else {
            return inputStream;
        }
    }

    public Properties getProps() {
        return props;
    }

}

package com.pokeme.utils;

public class ProdAndLocalConstants {

    public static String LOGIN_HOST;

    static {
        boolean local = false;
        if (local) {
            LOGIN_HOST = "http://localhost:8081";
        }
        else {
            LOGIN_HOST = "http://alexloginthingy.us-east-2.elasticbeanstalk.com";
        }
    }
}

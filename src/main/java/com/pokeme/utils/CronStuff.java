package com.pokeme.utils;

import com.cronutils.descriptor.CronDescriptor;
import com.cronutils.model.Cron;
import com.cronutils.model.CronType;
import com.cronutils.model.definition.CronDefinition;
import com.cronutils.model.definition.CronDefinitionBuilder;
import com.cronutils.parser.CronParser;

import java.util.Locale;

public class CronStuff {

    static CronDefinition cronDefinition =
            CronDefinitionBuilder.instanceDefinitionFor(CronType.QUARTZ);
    static CronParser parser = new CronParser(cronDefinition);

    public static String descriptionOfCronExpression(String cronExpression) {
        Cron quartzCron = parser.parse(cronExpression);
        CronDescriptor descriptor = CronDescriptor.instance(Locale.US);
        return descriptor.describe(quartzCron);
    }

}

package com.pokeme.utils;

import com.pokeme.database.models.Notification;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.*;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class JsonParser {

    private List<Notification> notifications = new ArrayList<>();

    public JsonParser() {
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader("notifications.json"))
        {
            Object obj = jsonParser.parse(reader);
            JSONObject main = (JSONObject) obj;
            JSONArray notifications = (JSONArray) main.get("notifications");
            notifications.forEach(notification -> parseNotifications((JSONObject)notification));

        } catch (IOException | ParseException e) {
            Log.error(JsonParser.class, String.format("Error Parsing JSON File: %s", e.getStackTrace().toString()));
        }
    }

    private void parseNotifications(JSONObject jsonNotification) {
        Notification notification = new Notification();
        notification.setId(UUID.randomUUID().toString());
        notification.setManager((String)jsonNotification.get("manager"));
        notification.setText((String)jsonNotification.get("text"));
        if (jsonNotification.get("media") != null) {
            notification.setMedia((String)jsonNotification.get("media"));
        }
        if (jsonNotification.get("description") != null) {
            notification.setDescription((String) jsonNotification.get("description"));
        }
        notification.setFrequency((String)jsonNotification.get("frequency"));
        List<String> numbers = new ArrayList<>();
        ((JSONArray)jsonNotification.get("phoneNumbers")).forEach(num -> numbers.add((String)num));
        notification.setPhoneNumbers(numbers);
        notifications.add(notification);
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

}


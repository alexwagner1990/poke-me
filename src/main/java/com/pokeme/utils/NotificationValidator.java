package com.pokeme.utils;

import com.pokeme.database.models.Notification;
import org.springframework.stereotype.Component;

import javax.validation.*;
import java.util.Set;
import java.util.UUID;

@Component
public class NotificationValidator {

    Validator validator;
    Set<ConstraintViolation<Notification>> constraintViolations;

    public NotificationValidator() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    public Notification isNotificationValid(Notification notification) {
        if (notification.getId() == null) {
            notification.setId(UUID.randomUUID().toString());
        }
        constraintViolations = validator.validate(notification);
        if (constraintViolations.size() != 0) {
            throw new ConstraintViolationException(constraintViolations);
        }
        return notification;
    }

    public Set<ConstraintViolation<Notification>> getLatestViolations() {
        return constraintViolations;
    }
}

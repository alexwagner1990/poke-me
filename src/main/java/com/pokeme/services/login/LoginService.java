package com.pokeme.services.login;

import com.alexlogin.database.models.Token;
import com.pokeme.SpaWebFilter;
import com.pokeme.api.errors.ApiError;
import com.pokeme.utils.Log;
import com.pokeme.utils.ProdAndLocalConstants;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class LoginService {

    private final WebClient webClient;
    private static final String LOGIN_HOST = ProdAndLocalConstants.LOGIN_HOST;

    public LoginService(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.baseUrl(LOGIN_HOST).build();
    }

    public Token getToken(String auth, String applicationCode) {
        Mono<Token> tokenMono = webClient.get()
                .uri("/v1/token")
                .header("Authorization", auth)
                .header("App-Code", applicationCode)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, clientResponse -> {
                    Log.info(SpaWebFilter.class, "ERROR: " + clientResponse.toString());
                    return clientResponse.bodyToMono(ApiError.class);
                })
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> {
                    Log.info(SpaWebFilter.class, "ERROR: " + clientResponse.toString());
                    return clientResponse.bodyToMono(ApiError.class);
                })
                .bodyToMono(Token.class);
        Token token = tokenMono.block();
        return token;
    }
}

package com.pokeme.services.timing;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;

import java.util.Objects;

public class JobAndTrigger {

    private String id;
    private JobDetail job;
    private CronTrigger cronTrigger;

    public JobAndTrigger(JobDetail job, CronTrigger cronTrigger) {
        this.job = job;
        this.cronTrigger = cronTrigger;
        this.id = job.getKey().getName();
    }
    public JobAndTrigger(String id) {
        this.id = id;
    }

    public JobDetail getJob() {
        return job;
    }

    public void setJob(JobDetail job) {
        this.job = job;
    }

    public CronTrigger getCronTrigger() {
        return cronTrigger;
    }

    public void setCronTrigger(CronTrigger cronTrigger) {
        this.cronTrigger = cronTrigger;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JobAndTrigger that = (JobAndTrigger) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

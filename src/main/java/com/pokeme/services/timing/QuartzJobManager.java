package com.pokeme.services.timing;

import com.pokeme.database.models.Notification;
import com.pokeme.utils.Log;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

public class QuartzJobManager {

    public static QuartzJobManager quartzJobManager;
    private List<JobAndTrigger> jobsAndTriggers;
    private Scheduler scheduler;

    private QuartzJobManager() {
        jobsAndTriggers = new ArrayList<>();
        try {
            SchedulerFactory schedulerFactory = new StdSchedulerFactory();
            scheduler = schedulerFactory.getScheduler();
            scheduler.start();
            quartzJobManager = this;
        }
        catch (Exception e) {
            Log.error(QuartzJobManager.class, String.format("Problem with scheduler: %s", e.getStackTrace().toString()));
        }
    }

    public static QuartzJobManager getInstance() {
        if (quartzJobManager == null) {
            quartzJobManager = new QuartzJobManager();
        }
        return quartzJobManager;
    }

    public void scheduleJobs() {
        jobsAndTriggers.forEach(jobAndTrigger -> {
            try {
                scheduler.scheduleJob(jobAndTrigger.getJob(), jobAndTrigger.getCronTrigger());
            }
            catch (SchedulerException e) {
                Log.error(QuartzJobManager.class, String.format("Error with Scheduling jobs: %s", e.getMessage()));
            }
        });
    }

    public void scheduleJob(JobAndTrigger jandt) {
        try {
            scheduler.scheduleJob(jandt.getJob(), jandt.getCronTrigger());
        }
        catch (SchedulerException e) {
            Log.error(QuartzJobManager.class, String.format("Error with Scheduling jobs: %s", e.getMessage()));
        }
    }

    public void buildJobsAndTriggers(List<Notification> notifications) {
        notifications.forEach((notification) -> {
            buildJobAndTrigger(notification);
        });
    }

    public JobAndTrigger buildJobAndTrigger(Notification notification) {
        JobDetail job = buildJob(notification);
        CronTrigger trigger = buildTrigger(notification);
        JobAndTrigger jobAndTrigger = new JobAndTrigger(job, trigger);
        jobsAndTriggers.add(jobAndTrigger);
        return jobAndTrigger;
    }

    public JobDetail buildJobDetail(Notification notification) {
        return buildJob(notification);
    }

    public Trigger buildTriggerDetail(Notification notification) {
        return buildTrigger(notification);
    }

    public boolean stopAndDeleteJob(String notificationId) {
        try {
            return scheduler.deleteJob(JobKey.jobKey(notificationId));
        } catch (SchedulerException e) {
            Log.error(QuartzJobManager.class, String.format("Problem stopping job: %s", e.getMessage()));
            return false;
        }
    }

    private JobDetail buildJob(Notification notification) {
        return JobBuilder.newJob(SendMessageJob.class)
                .withIdentity(notification.getId())
                .usingJobData("text", notification.getText())
                .usingJobData("manager", notification.getManager())
                .usingJobData("cron", notification.getFrequency())
                .usingJobData("media", notification.getMedia())
                .usingJobData("phoneNumbers", String.join(",", notification.getPhoneNumbers()))
                .storeDurably(false)
                .build();
    }

    public CronTrigger buildTrigger(Notification notification) {
        return TriggerBuilder.newTrigger()
                .withIdentity("Trigger_" + notification.getId())
                .withSchedule(CronScheduleBuilder.cronSchedule(notification.getFrequency())
                        .inTimeZone(TimeZone.getTimeZone(notification.getTimezone() == null || notification.getTimezone().isBlank() ? "America/New_York" : notification.getTimezone())))
                .build();
    }

    public void removeJobFromManager(Notification notification) {
        jobsAndTriggers.remove(new JobAndTrigger(notification.getId()));
    }

    public Scheduler getScheduler() {
        return scheduler;
    }

}

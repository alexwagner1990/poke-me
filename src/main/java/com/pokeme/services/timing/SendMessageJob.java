package com.pokeme.services.timing;

import com.pokeme.ApplicationContextProvider;
import com.pokeme.database.models.Notification;
import com.pokeme.managers.TwilioMongoManager;
import com.pokeme.services.mongo.NotificationService;
import com.pokeme.services.mongo.NotificationServiceImpl;
import com.pokeme.services.twilio.messages.TwilioMessageBuilder;
import com.pokeme.services.twilio.connection.TwilioConnection;
import com.twilio.rest.api.v2010.account.MessageCreator;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import java.util.List;

public class SendMessageJob implements Job {

    @Override
    public void execute(JobExecutionContext context) {
        String id = context.getJobDetail().getKey().getName();
        NotificationService service = ApplicationContextProvider.getApplicationContext().getBean(NotificationServiceImpl.class);
        Notification not = service.findById(id);
        TwilioMessageBuilder builder = new TwilioMessageBuilder();
        List<MessageCreator> creatorList = builder.createMessage(not);
        creatorList.forEach(creator -> {
            TwilioConnection connection = TwilioConnection.getInstance();
            connection.sendMessage(creator);
        });
        if (not.getInstances() != null && not.getInstances().equalsIgnoreCase("true")) {
            TwilioMongoManager twilioMongoManager = ApplicationContextProvider.getApplicationContext().getBean(TwilioMongoManager.class);
            twilioMongoManager.deleteNotification(id);
        }
    }
}

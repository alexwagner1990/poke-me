package com.pokeme.services.mongo;

import com.pokeme.database.models.Notification;

import java.util.List;

public interface NotificationService {
    List<Notification> findAll();

    List<Notification> findByManager(String manager);

    Notification findById(String id);

    Notification saveOrUpdateNotification(Notification notification);

    boolean deleteNotificationById(String id);
}

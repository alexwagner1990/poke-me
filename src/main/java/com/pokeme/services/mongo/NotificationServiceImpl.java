package com.pokeme.services.mongo;

import com.pokeme.database.models.Notification;
import com.pokeme.database.repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationServiceImpl implements NotificationService {

    @Autowired
    private NotificationRepository notificationRepository;

    @Override
    public List<Notification> findAll() {
        return notificationRepository.findAll();
    }

    @Override
    public List<Notification> findByManager(String manager) {
        return notificationRepository.findByManager(manager);
    }

    @Override
    public Notification findById(String id) {
        return notificationRepository.findById(id).orElse(null);
    }

    @Override
    public Notification saveOrUpdateNotification(Notification notification) {
        return notificationRepository.save(notification);
    }

    @Override
    public boolean deleteNotificationById(String id) {
        notificationRepository.deleteById(id);
        return true;
    }
}

package com.pokeme.services.email;

public interface EmailService {

    void sendEmail();
}

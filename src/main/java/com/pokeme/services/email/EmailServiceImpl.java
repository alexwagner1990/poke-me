package com.pokeme.services.email;

import com.pokeme.utils.ConfigProps;
import org.springframework.stereotype.Service;

import java.util.Properties;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;


@Service
public class EmailServiceImpl implements EmailService {

    public static Properties buildProps() {
        Properties props = new Properties();
        Properties config = new ConfigProps().getProps();
//        props.put("mail.host", "smtpout.secureserver.net");
//        props.put("mail.port", "465");
//        props.put("mail.username", "info@textmelatergator.com");
//        props.put("mail.password", "Bearandbat1825!");
//        props.put("mail.protocol", "smtp");

        props.put("mail.host", "smtp.gmail.com");
        props.put("mail.port", "465");
        props.put("mail.username", config.get("email"));
        props.put("mail.name", "Alex Wagner");
        props.put("mail.password", config.get("emailpw"));
        props.put("mail.protocol", "smtp");

        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.ssl.enable", "true");
        props.put("mail.smtp.ssl.trust", "*");

        props.put("mail.smtp.connectiontimeout", "60000");
        props.put("mail.smtp.timeout", "60000");

        return props;
    }

    public void sendEmail() {
        Session session = Session.getInstance(buildProps(), new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("alexw123090@gmail.com", "yzxxoeuphfrjprzq");
            }
        });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("info@textmelatergator.com"));
            message.setRecipients(
                    Message.RecipientType.TO, InternetAddress.parse("alexw123090@gmail.com"));
            message.setSubject("Mail Subject");

            String msg = "This is my first email using JavaMailer";

            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(msg, "text/html; charset=utf-8");

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(mimeBodyPart);

            message.setContent(multipart);
            Transport.send(message);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}

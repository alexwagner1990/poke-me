package com.pokeme.services.twilio.messages;

import com.pokeme.database.models.Notification;
import com.pokeme.utils.CronStuff;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.api.v2010.account.MessageCreator;
import com.twilio.type.PhoneNumber;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TwilioMessageBuilder {

    public List<MessageCreator> createMessage(Notification notification) {
        List<MessageCreator> creators = new ArrayList<>();
        if (notification == null) {
            return creators;
        }
        notification.getPhoneNumbers().forEach(number -> {
            MessageCreator creator = Message.creator(
                    new PhoneNumber(number),
                    new PhoneNumber("+16405003593"),
                    notification.getText()
            );
            if (notification.getMedia() != null && !notification.getMedia().isBlank()) {
                creator.setMediaUrl(Arrays.asList(URI.create(notification.getMedia())));
            }
            creators.add(creator);
        });
        return creators;
    }

    public List<MessageCreator> createWelcomeMessage(List<Notification> notifications) {
        List<MessageCreator> creators = new ArrayList<>();
        notifications.forEach(notification -> {
            notification.getPhoneNumbers().forEach(number -> {
                MessageCreator creator = Message.creator(
                        new PhoneNumber(number),
                        new PhoneNumber("+16405003593"),
                        welcomeMessageText(notification)
                ).setMediaUrl(Arrays.asList(URI.create("https://media.giphy.com/media/28GHfhGFWpFgsQB4wR/giphy.gif")));
                creators.add(creator);
            });
        });
        return creators;
    }

    public List<MessageCreator> createEditMessage(Notification notification) {
        List<MessageCreator> creators = new ArrayList<>();
            notification.getPhoneNumbers().forEach(number -> {
                MessageCreator creator = Message.creator(
                        new PhoneNumber(number),
                        new PhoneNumber("+16405003593"),
                        editMessageText(notification)
                ).setMediaUrl(Arrays.asList(URI.create("https://media.giphy.com/media/453QsWPQj5bsQaqp8M/giphy.gif")));
                creators.add(creator);
            });
        return creators;
    }

    private String welcomeMessageText(Notification notification) {
        StringBuilder builder = new StringBuilder();
        builder.append("Hi! You're receiving this text because you signed up for notifications! ");
        builder.append("You'll get notifications about \"");
        builder.append(notification.getDescription());
        builder.append("\" at the ");
        if ("true".equalsIgnoreCase(notification.getInstances())) {
            builder.append("next instance of \"");
        }
        else {
            builder.append("cadence of \"");
        }
        if (notification.getFrequency() != null && !notification.getFrequency().isEmpty()) {
            builder.append(CronStuff.descriptionOfCronExpression(notification.getFrequency()));
        }
        builder.append("\". If this isn't right or you want them to stop, complain to Alex about it!");
        return builder.toString();
    }

    private String editMessageText(Notification notification) {
        StringBuilder builder = new StringBuilder();
        builder.append("You have successfully edited your notification about ");
        builder.append(notification.getDescription());
        builder.append("\".");
        return builder.toString();
    }

}

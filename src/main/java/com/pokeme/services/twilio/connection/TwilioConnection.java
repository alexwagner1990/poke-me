package com.pokeme.services.twilio.connection;

import com.pokeme.utils.Log;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.api.v2010.account.MessageCreator;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TwilioConnection {

    public static final String ACCOUNT_SID = "AC7c6ee5ada0ea7f88101278a0a6713729";
    public static final String ACCOUNT_TOKEN = "b45e2bd69c4e9eecf243cbc68a91d830";
    public static TwilioConnection connection;

    private TwilioConnection() {
        Twilio.init(ACCOUNT_SID, ACCOUNT_TOKEN);
    }

    public static TwilioConnection getInstance() {
        if (connection == null) {
            connection = new TwilioConnection();
        }
        return connection;
    }

    public void sendMessage(MessageCreator creator) {
        Message message = creator.create();
        Log.info(TwilioConnection.class, (String.format("Message Sent: %s", message.getAccountSid())));
    }

    public void sendMessages(List<MessageCreator> creators) {
        creators.forEach(this::sendMessage);
    }
}

package com.pokeme;

import com.pokeme.managers.TwilioMongoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories
public class PokeMeApplication implements CommandLineRunner {

	@Autowired
	TwilioMongoManager twilioManager;

	public static void main(String[] args) {
		SpringApplication.run(PokeMeApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		twilioManager.startSavedNotifications();
	}

}

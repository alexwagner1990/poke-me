package com.pokeme.managers;

import com.pokeme.database.models.Notification;
import com.pokeme.services.mongo.NotificationService;
import com.pokeme.services.twilio.connection.TwilioConnection;
import com.pokeme.services.twilio.messages.TwilioMessageBuilder;
import com.pokeme.services.timing.JobAndTrigger;
import com.pokeme.services.timing.QuartzJobManager;
import com.pokeme.utils.Log;
import com.twilio.rest.api.v2010.account.MessageCreator;
import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class TwilioMongoManager {

    @Autowired
    TwilioConnection connection;
    @Autowired
    NotificationService notificationService;

    public Notification createNewNotification(Notification notification) {
        notification.setFrequencyDescription();
        sendWelcomeMessages(Arrays.asList(notification));
        scheduleJobs(Arrays.asList(notification));
        return notificationService.saveOrUpdateNotification(notification);
    }

    public Notification editNotification(Notification notification) {
        notification.setFrequencyDescription();
        QuartzJobManager manager = QuartzJobManager.getInstance();
        JobDetail detail = manager.buildJobDetail(notification);
        deleteJob(notification.getId());
        manager.removeJobFromManager(notification);
        manager.scheduleJob(new JobAndTrigger(detail, manager.buildTrigger(notification)));
        sendEditMessages(notification);
        return notificationService.saveOrUpdateNotification(notification);
    }

    public void startSavedNotifications() {
        List<Notification> allNotifications = notificationService.findAll();
        allNotifications.forEach(notification -> Log.info(TwilioMongoManager.class, notification.toString()));
        scheduleJobs(allNotifications);
    }

    public boolean deleteNotification(String id) {
        deleteJob(id);
        return notificationService.deleteNotificationById(id);
    }

    private void sendWelcomeMessages(List<Notification> notifications) {
        TwilioMessageBuilder builder = new TwilioMessageBuilder();
        List<MessageCreator> creators = builder.createWelcomeMessage(notifications);
        connection.sendMessages(creators);
    }

    private void sendEditMessages(Notification notification) {
        TwilioMessageBuilder builder = new TwilioMessageBuilder();
        List<MessageCreator> creators = builder.createEditMessage(notification);
        connection.sendMessages(creators);
    }

    private boolean deleteJob(String id) {
        QuartzJobManager scheduler = QuartzJobManager.getInstance();
        return scheduler.stopAndDeleteJob(id);
    }

    private void scheduleJobs(List<Notification> myNotifications) {
        QuartzJobManager scheduler = QuartzJobManager.getInstance();
        scheduler.buildJobsAndTriggers(myNotifications);
        scheduler.scheduleJobs();
    }

    private void scheduleJob(Notification myNotification) {
        QuartzJobManager scheduler = QuartzJobManager.getInstance();
        JobAndTrigger jandt = scheduler.buildJobAndTrigger(myNotification);
        scheduler.scheduleJob(jandt);
    }
}

package com.pokeme.managers;

import com.pokeme.database.models.Notification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

@Component
public class TokenManager {

    public void canUserAccessResource(String userid, Notification notification) throws ResponseStatusException {
        if (!userid.equals(notification.getManager())) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Token not for given resource");
        }
    }

}

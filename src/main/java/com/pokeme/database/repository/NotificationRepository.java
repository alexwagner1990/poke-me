package com.pokeme.database.repository;

import com.pokeme.database.models.Notification;
import org.springframework.data.mongodb.core.aggregation.BooleanOperators;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface NotificationRepository extends MongoRepository<Notification, String> {

    @Query("{id:'?0'}")
    Optional<Notification> findById(String id);

    @Query("{manager:'?0'}")
    List<Notification> findByManager(String manager);

    public long count();
}

package com.pokeme.database.dto;

import com.pokeme.database.models.Notification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class NotificationDTO {

    private String id;
    private String manager;
    private String text;
    private String media;
    private String frequency;
    private String description;
    private String phoneNumbers;
    private String instances;
    private String type;
    private String timezone;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(String phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public String getInstances() {
        return instances;
    }

    public void setInstances(String instances) {
        this.instances = instances;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public List<String> convertPhoneNumbers(String phoneNumbers) {
        List<String> numbers = new ArrayList<>();
        if (phoneNumbers != null) {
            numbers.addAll(Arrays.asList(phoneNumbers.split(",")));
        }
        return numbers;
    }

    public Notification convertToNotification() {
        Notification notification = new Notification(
                this.id != null ? this.id : UUID.randomUUID().toString(),
                this.manager,
                this.text,
                this.media,
                this.frequency,
                this.description,
                convertPhoneNumbers(this.phoneNumbers),
                this.instances,
                this.type,
                this.timezone
        );
        return notification;
    }
}

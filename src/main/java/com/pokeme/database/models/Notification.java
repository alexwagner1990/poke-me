package com.pokeme.database.models;

import com.pokeme.utils.CronStuff;
import org.quartz.JobDataMap;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@Document(collection = "notifications")
public class Notification implements Serializable {

    private String id;
    // This should be equal to the user's ID
    private String manager;
    private String text;
    private String media;
    private String frequency;
    private String frequencyDescription;
    private String description;
    private String instances;
    private String type;
    private String timezone;
    private List<String> phoneNumbers;

    public Notification() {}

    public Notification(String id) {
        this.id = id;
    }

    public Notification(String id, String manager, String text, String media, String frequency, String description, List<String> phoneNumbers, String instances, String type, String timezone) {
        this.id = id;
        this.manager = manager;
        this.text = text;
        this.media = media;
        this.frequency = frequency;
        this.description = description;
        this.phoneNumbers = phoneNumbers;
        this.instances = instances;
        this.type = type;
        this.timezone = timezone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getFrequencyDescription() {
        return frequencyDescription;
    }

    public void setFrequencyDescription(String frequency) {
        this.frequencyDescription = frequency;
    }

    public void setFrequencyDescription() {
        this.frequencyDescription = CronStuff.descriptionOfCronExpression(this.frequency);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public String getInstances() {
        return instances;
    }

    public void setInstances(String instances) {
        this.instances = instances;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public void buildFromJobMap(JobDataMap dataMap) {
        this.frequency = dataMap.getString("cron");
        this.manager = dataMap.getString("manager");
        this.media = (dataMap.getString("media"));
        this.text = (dataMap.getString("text"));
        this.phoneNumbers = (Arrays.asList(dataMap.getString("phoneNumbers").split(",")));
    }

    @Override
    public String toString() {
        return "Notification{" +
                "id='" + id + '\'' +
                ", manager='" + manager + '\'' +
                ", text='" + text + '\'' +
                ", media='" + media + '\'' +
                ", frequency='" + frequency + '\'' +
                ", description='" + description + '\'' +
                ", phoneNumbers=" + phoneNumbers +
                '}';
    }
}

package com.pokeme.controllers.rest;

import com.pokeme.database.models.Notification;
import com.pokeme.managers.TokenManager;
import com.pokeme.managers.TwilioMongoManager;
import com.pokeme.services.email.EmailService;
import com.pokeme.services.mongo.NotificationService;
import com.pokeme.utils.NotificationValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/notification")
public class NotificationRestController {

    @Autowired
    TwilioMongoManager twilioManager;
    @Autowired
    NotificationValidator notificationValidator;
    @Autowired
    NotificationService notificationService;
    @Autowired
    TokenManager tokenManager;
    @Autowired
    EmailService emailService;

    @PostMapping
    @ResponseBody
    public Notification createNotification(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorization, @RequestBody Notification notification, @RequestAttribute String userid) {
        notification.setManager(userid);
        notification = notificationValidator.isNotificationValid(notification);
        return twilioManager.createNewNotification(notification);
    }

    @PostMapping("/emailtest")
    public void sendEmail(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorization) {
        emailService.sendEmail();
    }

    @PutMapping
    @ResponseBody
    public Notification changeNotification(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorization, @RequestBody Notification notification, @RequestAttribute String userid) {
        notification.setManager(userid);
        tokenManager.canUserAccessResource(userid, notification);
        notification = notificationValidator.isNotificationValid(notification);
        return twilioManager.editNotification(notification);
    }

    @GetMapping
    @ResponseBody
    public List<Notification> getNotifications(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorization, @RequestAttribute String userid) {
        return notificationService.findByManager(userid);
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Notification getNotification(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorization, @PathVariable String id, @RequestAttribute String userid) {
        Notification notification = notificationService.findById(id);
        tokenManager.canUserAccessResource(userid, notification);
        return notification;
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    public boolean deleteNotification(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorization, @PathVariable String id, @RequestAttribute String userid) {
        getNotification(authorization, id, userid);
        return twilioManager.deleteNotification(id);
    }
}

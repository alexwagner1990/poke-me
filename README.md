## About

This is (or aims to be) an accessible, API first, http tool for setting and sending text reminders on a designated schedule. It has a relatively basic frontend but can also be used as an API. 

## How to Run

The project is packaged to run as a single JAR (the Spring backend included with the react frontend) - mvn spring-boot:run at the project level starts the project as this jar. If the frontend alone is to be run, npm start can be run at the frontend/ directory to run the frontend (which will communicate with the backend from mvn spring-boot:run), which is recommended for development as it will reflect frontend changes on refresh. 

## Deployment

The app can be deployed as a single JAR (backend and frontend) with mvn -U clean package. Accessing the app requires the (login service)[https://gitlab.com/alexwagner1990/my-login-service] to be running as well. The app also requires the (email service)[https://gitlab.com/alexwagner1990/my-email-tool] as a dependency.

Currently the app lives at http://textmelatergator.com, although it is currently disabled.

## Next Steps

- Add different kinds of alerts like email, slack, etc.
- Make the site less spartan-looking
